<?php
ob_start();
session_start();


class Banners_Model extends Dao
{
	
	private $nome_tabela = "tb_banners";
	private $chave_tabela = "idbanner";
	public $obj_imagem;
	
	
	/*	==================================================================================================================	*/
	#	CONSTRUTOR DA CLASSE
	/*	==================================================================================================================	*/
	public function __construct()
	{
		$this->obj_imagem = new Imagem();
		parent::__construct();
	}
    
    
    
    
    #-----------------------------------------------------------------------------------------------------------------------#
    #   GERA UM SELECT COM TODAS AS UFS
    #-----------------------------------------------------------------------------------------------------------------------#
    public function tipo_banners($name, $valor, $class = '', $titulo = "Selecione", $complemento_javascript = "")
    {
          $obj_dao = new Dao();
          //lista de ufs
          $valor = trim($valor);
          $lista = array(
                        array('id' => '1', 'titulo' => "Banner Principal"),
                        array('id' => '2', 'titulo' => "Mobile - Banners principal"),
                        );
        echo '<select name="'.$name.'" id="'.$name.'" class="'.$class.'">';
        echo "<option value=''>$titulo</option>";
        for($i=0; $i < count($lista); $i++)
        {
            //verica se há na lista e check em caso afirmativo
            if($valor == $lista[$i][id])
            {
                ?>
                   <option value="<?php Util::imprime($lista[$i][id]); ?>" selected="selected"><?php Util::imprime($lista[$i][titulo]); ?></option>
                <?php
            }
            else
            {
                ?>
                <option value="<?php Util::imprime($lista[$i][id]); ?>"><?php Util::imprime($lista[$i][titulo]); ?></option>
                <?php
            }
        }
        echo '</select>';
    }

	
	
	
	/*	==================================================================================================================	*/
	/*	FORMULARIO	*/
	/*	==================================================================================================================	*/
	public function formulario($dados)
	{
		$obj_jquery = new Biblioteca_Jquery();
	?>
    	<script language="javascript"> 
			$(document).ready(function() {				
				jQuery(function($){
				   $("#data_atendimento").mask("99/99/9999");
				});
			});
		</script>
        
        
        
        
        <div class="class-form-1">
            <ul>
                <li>
                    <p>Tipo de banner</p>
                    <?php $this->tipo_banners('tipo_banner', $dados[tipo_banner], 'form-control fundo-form1 input100') ?>
                </li>

            	<li>
                	<p>Título<span></span></p>
                    <input type="text" name="titulo" id="titulo" value="<?php Util::imprime($dados[titulo]) ?>" class="validate(required)" />
                </li>
                <li>
                	<p>Imagem</p>
                    <input type="file" name="imagem" id="imagem" value="<?php Util::imprime($dados[imagem]) ?>" />
                </li>
            </ul>
            <?php
			if(!empty($dados['imagem']))
			{
				?>
				<p>Atual:</p>
				<img src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[imagem]) ?>" style="max-width:690px;" />
				<?php
			}
			?>
        </div>
        
    <?php
	}
	
	/*	==================================================================================================================	*/
	/*	EFETUA CROP DA IMAGEM	*/
	/*	==================================================================================================================	*/
	public function efetua_crop_imagem($id, $nome_arquivo, $nome_campo, $tamanho_width_tumb, $tamanho_height_tumb, $url_retorno, $msg_sucesso, $tamanho_imagem = 980)
	{
		//	CRIO O CROP DA IMAGEM
		$nome_tabela = $this->nome_tabela;
		$idtabela = $this->chave_tabela;		
		$this->obj_imagem->gera_imagem_crop($id, $nome_arquivo, $nome_tabela, $idtabela, $nome_campo, $tamanho_imagem, $tamanho_width_tumb, $tamanho_height_tumb, $url_retorno, $msg_sucesso);	
	}
	
	
	
	/*	==================================================================================================================	*/
	/*	EFETUA O CADASTRO	*/
	/*	==================================================================================================================	*/
	public function cadastra($dados)
	{
		
		
		
		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "CADASTRO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);
		
		//	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
		if($_FILES[imagem][name] != "")
		{
			//	EFETUO O UPLOAD DA IMAGEM
			$dados[imagem] = Util::upload_imagem("../../uploads", $_FILES[imagem], "3145728");	
			
			//	EFETUO O CROP DA IMAGEM
			//$this->efetua_crop_imagem($id, $nome_arquivo, "imagem", 980, 450, $_SERVER['PHP_SELF'], "Cadastro efetuado com sucesso");
		}
		
		
		//	CADASTRA O USUARIO
		
		$id = parent::insert($this->nome_tabela, $dados);
		
		
		Util::script_msg("Cadastro efetuado com sucesso.");
		Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/cadastra.php");
	}
	
	
	
	/*	==================================================================================================================	*/
	/*	EFETUA A ALTERACAO	*/
	/*	==================================================================================================================	*/
	public function altera($id, $dados)
	{
		
		
		
		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "ALTERAÇÃO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);
		
		//	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
		if($_FILES[imagem][name] != "")
		{
			//	EFETUO O UPLOAD DA IMAGEM
			$dados[imagem] = Util::upload_imagem("../../uploads", $_FILES[imagem], "3145728");	
			
			//	EFETUO O CROP DA IMAGEM
			//$this->efetua_crop_imagem($id, $nome_arquivo, "imagem", 980, 450, $_SERVER['PHP_SELF'], "Cadastro efetuado com sucesso");
		}
		
		
		parent::update($this->nome_tabela, $id, $dados);
		
		
	}
	
	
	
	/*	==================================================================================================================	*/
	/*	ATIVA OU DESATIVA	*/
	/*	==================================================================================================================	*/
	public function ativar_desativar($id, $ativo)
	{
		if($ativo == "SIM")
		{
			$sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'NAO' WHERE " . $this->chave_tabela. " = '$id'";
			parent::executaSQL($sql);
			
			//	ARMAZENA O LOG
			parent::armazena_log("tb_logs_logins", "DESATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
		}
		else
		{
			$sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'SIM' WHERE " . $this->chave_tabela. " = '$id'";
			parent::executaSQL($sql);
			
			//	ARMAZENA O LOG
			parent::armazena_log("tb_logs_logins", "ATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
		}
		
	}
	
	
	
	
	/*	==================================================================================================================	*/
	/*	EXCLUI	*/
	/*	==================================================================================================================	*/
	public function excluir($id)
	{
		//	BUSCA OS DADOS
		$row = $this->select($id);
		
		$sql = "DELETE FROM " . $this->nome_tabela. " WHERE " . $this->chave_tabela. " = '$id'";
		parent::executaSQL($sql);
		
		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "EXCLUSÃO DO LOGIN $id, NOME: $row[nome], Email: $row[email]", $sql, $_SESSION[login][idlogin]);
	}
	
	
	
	
	
	
	
	/*	==================================================================================================================	*/
	/*	VERIFICO SE JA POSSUI O GRUPO CADASTRADO	*/
	/*	==================================================================================================================	*/
	public function verifica($email)
	{
		$sql = "SELECT * FROM " . $this->nome_tabela. " WHERE email = '$email'";
		return mysql_num_rows(parent::executaSQL($sql));
	}
	
	
	
	
	/*	==================================================================================================================	*/
	/*	VERIFICO SE JA POSSUI O GRUPO CADASTRADO QUANDO ALTERAR	*/
	/*	==================================================================================================================	*/
	public function verifica_altera($email, $id)
	{
			$sql = "SELECT * FROM " . $this->nome_tabela. " WHERE email = '$email' AND " . $this->chave_tabela. " <> '$id'";
		return mysql_num_rows(parent::executaSQL($sql));
	}
	
	
	

	
	/*	==================================================================================================================	*/
	/*	BUSCA OS DADOS	*/
	/*	==================================================================================================================	*/
	public function select($id = "")
	{
		if($id != "")
		{
			$sql = "
					SELECT
						*
					FROM
						" . $this->nome_tabela. "
					WHERE
						" . $this->chave_tabela. " = '$id'
					";
			return mysql_fetch_array(parent::executaSQL($sql));
		}
		else
		{
			$sql = "
				SELECT
					*
				FROM
					" . $this->nome_tabela. "
				ORDER BY
					ordem desc
				";
			return parent::executaSQL($sql);
		}
		
	}
	
	
	
	
	
	
	
}

?>
