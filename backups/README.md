cpanel-Fullbackup
=================

CPanel full backup (all files+databases+emails) PHP script

Working script to take full backup (all files+databases+emails) manually or using cron services on CPanel based hosting servers.
It needs CPanel account details and FTP host/account details. 

Most of the scripts for doing full backups are either old, totally unusable or commercial.
I wrote one for my own use and sharing here so others don’t need to re-invent the wheel.

If it works for you, share your comments.
