-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: mysql01.atacadaodamadeiradf.hospedagemdesites.ws
-- Generation Time: 22-Ago-2016 às 14:59
-- Versão do servidor: 5.6.30-76.3-log
-- PHP Version: 5.6.24-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atacadaodamade1`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `OLD_tb_banners`
--

CREATE TABLE `OLD_tb_banners` (
  `idbanner` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `imagem` varchar(80) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `tipo_banner` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `OLD_tb_banners`
--

INSERT INTO `OLD_tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`) VALUES
(1, 'Atacadão da Madeira', '1602201311166206473385.jpg', 'SIM', 0, 1),
(2, 'Trabalhamos com produtos de primeira qualidade', '1303201309519650078403.jpg', 'NAO', 0, 1),
(3, 'Compromisso em atender a consciência de preservação do meio ambiente.', '1303201309456178819508.jpg', 'SIM', 0, 1),
(5, 'Na hora de comprar madeira compre com com entende do ramo a mais de 16 anos', '1303201309475028432967.jpg', 'SIM', 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE `tb_banners` (
  `idbanner` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `imagem` varchar(80) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `tipo_banner` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`) VALUES
(1, 'Atacadão da Madeira', '1602201311166206473385.jpg', 'SIM', 0, 1),
(2, 'Trabalhamos com produtos de primeira qualidade', '1303201309519650078403.jpg', 'NAO', 0, 1),
(3, 'Compromisso em atender a consciência de preservação do meio ambiente.', '1303201309456178819508.jpg', 'SIM', 0, 1),
(5, 'Na hora de comprar madeira compre com com entende do ramo a mais de 16 anos', '1303201309475028432967.jpg', 'SIM', 0, 1),
(6, 'Banner mobile 1', '2506201602051349210548.jpg', 'SIM', 0, 2),
(7, 'Banner mobile 2', '2506201602051313123994.jpg', 'SIM', 0, 2),
(8, 'Banner mobile 3', '2506201602051163187017.jpg', 'SIM', 0, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE `tb_dicas` (
  `iddica` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `ativo`, `ordem`, `keywords`, `description`, `imagem`) VALUES
(4, 'As funções e partes de um telhado', '<p style="text-align: justify;">\r\n	Os telhados tem a fun&ccedil;&atilde;o de receber as &aacute;guas da chuva, proporcionar isolamento t&eacute;rmico e proteger de outros acontecimentos atmosf&eacute;ricos.</p>\r\n<p style="text-align: justify;">\r\n	Composto de telhas inclinadas colocadas de maneira a canalizar as &aacute;guas para o solo, tem tamb&eacute;m uma fun&ccedil;&atilde;o est&eacute;tica. &nbsp;Quando bem desenhado o telhado invariavelmente embeleza a casa.</p>\r\n<p style="text-align: justify;">\r\n	O telhado comp&otilde;e-se da estrutura, cobertura e dos condutores de &aacute;guas pluviais:</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	<strong>&ndash; A estrutura:</strong> &eacute; o elemento de apoio da cobertura, que pode ser: de madeira, met&aacute;lica, etc&hellip; geralmente constitu&iacute;da de tesouras, oit&otilde;es, pontaletes ou vigas, tendo a fun&ccedil;&atilde;o de receber e distribuir adequadamente as cargas verticais ao restante da edifica&ccedil;&atilde;o;</p>\r\n<p style="text-align: justify;">\r\n	<strong>&ndash; A cobertura:</strong> &eacute; o elemento de prote&ccedil;&atilde;o, que pode ser: cer&acirc;mico, de fibrocimento, alum&iacute;nio, de chapa galvanizada, etc. Constitu&iacute;do por telhas de diversos materiais (cer&acirc;mica, fibrocimento, concreto, a&ccedil;o, met&aacute;lica, cobre, entre outros) e dimens&otilde;es, tendo a fun&ccedil;&atilde;o de veda&ccedil;&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	<strong>&ndash; Os condutores:</strong> s&atilde;o para o escoamento conveniente das &aacute;guas de chuva e constituem-se de:</p>\r\n<p style="text-align: justify;">\r\n	calhas, coletores, rufos e rinc&otilde;es, s&atilde;o de chapas galvanizadas e de p.v.c., condutores verticais e acess&oacute;rios, com a fun&ccedil;&atilde;o de drenagem das &aacute;guas pluviais.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	<strong>Partes do telhado</strong></p>\r\n<p style="text-align: justify;">\r\n	<strong>&ndash; &Aacute;gua :</strong> superf&iacute;cie plana inclinada de um telhado;</p>\r\n<p style="text-align: justify;">\r\n	<strong>&ndash; Beiral: </strong>proje&ccedil;&atilde;o do telhado para fora do alinhamento da parede;</p>\r\n<p style="text-align: justify;">\r\n	<strong>&ndash; Cumeeira: </strong>aresta horizontal delimitada pelo encontro entre duas &aacute;guas que geralmente localizada na parte mais alta do telhado;</p>\r\n<p style="text-align: justify;">\r\n	<strong>&ndash; Espig&atilde;o: </strong>aresta inclinada delimitada pelo encontro entre duas &aacute;guas que formam um &acirc;ngulo saliente, isto &eacute;, o espig&atilde;o &eacute; um divisor de &aacute;gua;</p>\r\n<p style="text-align: justify;">\r\n	<strong>&ndash; Rinc&atilde;o:</strong> aresta inclinada delimitada pelo encontro entre duas &aacute;guas que formam um &acirc;ngulo reentrante, isto &eacute;, o rinc&atilde;o &eacute; um captador de &aacute;guas (tamb&eacute;m conhecido como &aacute;gua furtada;</p>\r\n<p style="text-align: justify;">\r\n	<strong>&ndash; Pe&ccedil;a complementar: </strong>&nbsp;componente cer&acirc;mico ou de qualquer outro material que permite a solu&ccedil;&atilde;o de detalhes do telhado, podendo ser usado em cumeeiras, rinc&otilde;es, espig&otilde;es e arremates em geral; pode ser tamb&eacute;m uma pe&ccedil;a especial destinada a promove a ventila&ccedil;&atilde;o e/ou ilumina&ccedil;&atilde;o do &aacute;tico ou, na inesist6encia de forro, do pr&oacute;prio ambiente da edifica&ccedil;&atilde;o;</p>\r\n<p style="text-align: justify;">\r\n	<strong>&ndash; Rufo:</strong> pe&ccedil;a complementar de arremate entre o telhado e uma parede;</p>\r\n<p style="text-align: justify;">\r\n	<strong>&ndash; Fiada: </strong>seq&uuml;&ecirc;ncia de telhas na dire&ccedil;&atilde;o da sua largura.</p>\r\n<p style="text-align: justify;">\r\n	<strong>&ndash; V&eacute;rtice: </strong>ponto de encontro da linha de cumeeira com uma linha de espig&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fonte:&nbsp;http://www.fazfacil.com.br/reforma-construcao/partes-do-telhado/</p>', 'SIM', 0, '', '', '1706201608013458339890.jpg'),
(5, 'Opções em assoalho de madeira', '<p style="text-align: justify;">\r\n	Por mais que se pense ao contr&aacute;rio, um assoalho de madeira ainda &eacute; o modelo sonhado para muitas resid&ecirc;ncias, pois este deixa o im&oacute;vel mais sofisticado e ainda confere maior conforto aos ambientes.</p>\r\n<p style="text-align: justify;">\r\n	Como sabemos, hoje &eacute; mais dif&iacute;cil se conseguir esta mordomia para o nosso lar, pois devido aos grandes transtornos causados pelo aquecimento solar, &eacute; grande a preocupa&ccedil;&atilde;o com os desmatamentos, ent&atilde;o este tipo de assoalho &eacute; cada vez mais dif&iacute;cil de conseguir, pois al&eacute;m da escassez do produto, os pre&ccedil;os tornam-se muito altos.</p>\r\n<p style="text-align: justify;">\r\n	Mas para quem mesmo assim ainda insiste em conseguir este conforto para o lar, existem os assoalhos de madeira de reflorestamento que s&atilde;o fornecidos em madeira de eucalipto tratada, que deixam os ambientes muito bonitos, sem causar tanto dano as nossas consci&ecirc;ncias e nem a natureza, pois como sabemos, o eucalipto &eacute; retirado com certo cuidado e em pouco tempo ele brota, desta forma a mata &eacute; rapidamente reposta na natureza.</p>\r\n<p style="text-align: justify;">\r\n	Sabemos que a grande preocupa&ccedil;&atilde;o &eacute; quanto &agrave; retirada desordenada de madeiras nas matas, por isso, se ela for feita como rege as leis, sem maiores riscos as esp&eacute;cies, fica mais f&aacute;cil a extra&ccedil;&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	Por isso, ao adquirir este tipo de assoalho, certifique-se de que a madeira tenha sido retirada das matas de forma correta, sem causar riscos ao meio ambiente.</p>', 'SIM', 0, '', '', '1706201608108218610754.jpg'),
(6, 'Dicas de Madeiras', '<p style="text-align: justify;">\r\n	<strong>MADEIRA UTILIZADA EM CANTEIRO DE OBRA:</strong></p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	a)Madeira roli&ccedil;a - Utilizada em constru&ccedil;&otilde;es simples para fazer andaimes e escoramento de lajes e vigas.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	b) Pontaletes - Madeira de pinus ou cedrinho, de terceira categoria, seca ao ar, espessura m&iacute;nima de 7.5 cm.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	c) T&aacute;buas e sarrafos - Madeira de pinus, cedrinho ou equivalente, de terceira categoria, seco ao ar, corretamente serrada ou de bitola quase uniforme.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	<strong>MADEIRA INADEQUADA PARA COBERURAS E TELHADOS:</strong></p>\r\n<p style="text-align: justify;">\r\n	N&atilde;o devem ser empregadas na estrutura pe&ccedil;as de madeira nas seguintes condi&ccedil;&otilde;es:</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	- Quando sofrerem esmagamentos ou outros danos que possam comprometer a seguran&ccedil;a da estrutura.</p>\r\n<p style="text-align: justify;">\r\n	- Quando apresentarem alto teor de umidade (madeira verde).</p>\r\n<p style="text-align: justify;">\r\n	- Quando surgirem defeitos como n&oacute;s soltos, n&oacute;s que abrangem grande parte da se&ccedil;&atilde;o transversal da pe&ccedil;a, fendas exageradas, arqueamento acentuado (forma de arco de um trecho inteiro).</p>\r\n<p style="text-align: justify;">\r\n	- Quando n&atilde;o se adaptarem perfeitamente nas liga&ccedil;&otilde;es.</p>\r\n<p style="text-align: justify;">\r\n	- Quando apresentarem sinais de deteriora&ccedil;&atilde;o, por ataque de fungos ou insetos.</p>\r\n<div>\r\n	&nbsp;</div>', 'SIM', 0, '', '', '1706201608168624143703.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE `tb_empresa` (
  `idempresa` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `imagem` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `keywords` longtext NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `imagem`, `descricao`, `ativo`, `ordem`, `keywords`, `description`) VALUES
(1, 'Empresa', '1602201312107677809687.jpg', '<p>\r\n	A <strong>Atacad&atilde;o da Madeira</strong> &eacute; uma empresa s&eacute;ria e pioneira no ramo de madeiras, estamos atuando no mercado de madeiras ha 16 anos. Destacamos pelos produtos de alt&iacute;ssima qualidade e uma pol&iacute;tica de pre&ccedil;os altamente competitiva em rela&ccedil;&atilde;o ao mercado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Procuramos manter sempre em disponibilidade em nosso estoque o material anunciado ao clientes al&eacute;m dos lan&ccedil;amentos feitos por nossos fornecedores. Para assim atender melhor ao cliente nas suas necessidades imediatas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Nossas Metas e Objetivos s&atilde;o: Satisfa&ccedil;&atilde;o total do cliente; Alta qualidade nas mercadorias; Bom atendimento e Pre&ccedil;os baixos.</strong></p>\r\n<div>\r\n	&nbsp;</div>', 'SIM', 0, 'atacadao, madeira', 'atacadao da madeira');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_enderecos`
--

CREATE TABLE `tb_enderecos` (
  `idendereco` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `telefone` varchar(45) NOT NULL,
  `latitude` varchar(45) NOT NULL,
  `longitude` varchar(45) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `endereco` varchar(80) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `cid` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_enderecos`
--

INSERT INTO `tb_enderecos` (`idendereco`, `titulo`, `telefone`, `latitude`, `longitude`, `ativo`, `ordem`, `endereco`, `uf`, `cid`) VALUES
(1, 'Paranoá', '(61) 3369-3330  -  (61) 3369-1331 - (61) 9918', '-16.693894', '-49.283681', 'SIM', 0, 'Av. Paranoá Qd 34 Conjunto 26 Lote 07 - Paranoá - DF', 'DF', '5534673650195675895'),
(2, '', '(61) 99187-9911', '', '', 'NAO', 0, 'WhatsApp', 'DF', '12083611117699175693');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galeria_produto`
--

CREATE TABLE `tb_galeria_produto` (
  `idgaleria` int(10) UNSIGNED NOT NULL,
  `id_produto` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_galeria_produto`
--

INSERT INTO `tb_galeria_produto` (`idgaleria`, `id_produto`, `imagem`, `ativo`, `ordem`) VALUES
(1, 1, '1602201303449145769683.jpg', 'SIM', 0),
(2, 1, '1602201303441317944534.jpg', 'SIM', 0),
(3, 1, '1602201303447909443092.jpg', 'SIM', 0),
(4, 2, '1602201303442540702935.jpg', 'SIM', 0),
(5, 2, '1602201303447004150955.jpg', 'SIM', 0),
(6, 2, '1602201303448713931346.jpg', 'SIM', 0),
(22, 7, '2802201304314335941072.jpg', 'SIM', 0),
(23, 7, '2802201304323035083657.jpg', 'SIM', 0),
(24, 7, '2802201304337780488915.jpg', 'SIM', 0),
(25, 7, '2802201304353660774673.jpg', 'SIM', 0),
(26, 7, '2802201304377618006029.jpg', 'SIM', 0),
(39, 8, '0403201311062324720662.jpg', 'SIM', 0),
(42, 10, '0403201311283880271418.jpg', 'SIM', 0),
(43, 10, '0403201311311891770346.jpg', 'SIM', 0),
(47, 9, '0403201312073770172984.jpg', 'SIM', 0),
(48, 8, '0403201312088164458804.jpg', 'SIM', 0),
(50, 8, '0403201312105379219365.jpg', 'SIM', 0),
(51, 8, '0403201312149361400513.jpg', 'SIM', 0),
(52, 13, '0403201303265116298446.jpg', 'SIM', 0),
(54, 3, '1706201606268801717104.jpg', 'SIM', 0),
(55, 3, '1706201606264674051639.jpg', 'SIM', 0),
(56, 3, '1706201606265869144391.jpg', 'SIM', 0),
(57, 3, '1706201606262411911748.jpg', 'SIM', 0),
(58, 3, '1706201606269741040099.jpg', 'SIM', 0),
(59, 3, '1706201606264243465790.jpg', 'SIM', 0),
(60, 3, '1706201606278299333506.jpg', 'SIM', 0),
(62, 6, '1706201606571124306598.jpg', 'SIM', 0),
(63, 6, '1706201606588404176003.jpg', 'SIM', 0),
(64, 6, '1706201606582062511030.jpg', 'SIM', 0),
(65, 6, '1706201606596003218988.jpg', 'SIM', 0),
(66, 6, '1706201606597701740049.jpg', 'SIM', 0),
(69, 11, '1706201607398211954426.jpg', 'SIM', 0),
(70, 5, '1706201608341545125101.jpg', 'SIM', 0),
(72, 5, '1706201608374884434818.jpg', 'SIM', 0),
(73, 3, '1706201608412723720057.jpg', 'SIM', 0),
(78, 14, '1706201608598673771222.jpg', 'SIM', 0),
(79, 7, '1706201609132303548521.jpg', 'SIM', 0),
(80, 7, '1706201609142865482299.jpg', 'SIM', 0),
(84, 15, '1706201609478008472443.jpg', 'SIM', 0),
(85, 17, '1706201610201949287839.jpg', 'SIM', 0),
(86, 17, '1706201610221354325661.jpg', 'SIM', 0),
(87, 15, '1706201610254577025667.jpg', 'SIM', 0),
(88, 18, '1706201610329570676088.jpg', 'SIM', 0),
(89, 18, '1706201610323039019513.jpg', 'SIM', 0),
(90, 18, '1706201610325079568661.jpg', 'SIM', 0),
(91, 18, '1706201610324733768014.jpg', 'SIM', 0),
(92, 18, '1706201610326660800443.jpg', 'SIM', 0),
(95, 19, '0107201611541947919813.jpg', 'SIM', 0),
(96, 19, '0107201611554203879183.jpg', 'SIM', 0),
(98, 19, '0107201612061328526896.jpg', 'SIM', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_grupos_logins`
--

CREATE TABLE `tb_grupos_logins` (
  `idgrupologin` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `imagem` varchar(45) DEFAULT NULL,
  `exibir_menu` varchar(3) DEFAULT 'SIM',
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_grupos_logins`
--

INSERT INTO `tb_grupos_logins` (`idgrupologin`, `nome`, `imagem`, `exibir_menu`, `ativo`) VALUES
(1, 'Administradores', NULL, 'SIM', 'SIM'),
(9, 'Contabilidade', NULL, 'SIM', 'SIM');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_grupos_logins_tb_paginas`
--

CREATE TABLE `tb_grupos_logins_tb_paginas` (
  `id_grupologin` int(11) NOT NULL,
  `id_pagina` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_grupos_logins_tb_paginas`
--

INSERT INTO `tb_grupos_logins_tb_paginas` (`id_grupologin`, `id_pagina`) VALUES
(1, 148),
(1, 149),
(1, 150),
(1, 151),
(1, 152),
(1, 153),
(1, 154),
(1, 155),
(1, 156),
(1, 157),
(1, 158),
(1, 159),
(1, 160),
(1, 161),
(1, 162),
(1, 163),
(1, 164),
(1, 165),
(1, 166),
(1, 167),
(1, 168),
(1, 169),
(1, 170),
(1, 171),
(1, 172),
(1, 173),
(1, 174),
(1, 175),
(1, 176),
(1, 177),
(1, 178),
(1, 179),
(1, 180),
(1, 181),
(1, 182),
(1, 183),
(1, 184),
(1, 185),
(1, 186),
(1, 187),
(1, 188),
(1, 189),
(1, 190),
(1, 191),
(1, 192),
(1, 193),
(1, 194),
(1, 195),
(1, 196),
(1, 197),
(1, 198),
(1, 199),
(1, 200),
(10, 148),
(10, 149),
(10, 150),
(10, 151),
(10, 152),
(10, 153),
(10, 154),
(10, 155),
(10, 156),
(10, 157),
(10, 158);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_info_empresa`
--

CREATE TABLE `tb_info_empresa` (
  `idinfo` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `telefone` varchar(45) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `google` varchar(255) NOT NULL,
  `msg_saudacao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_info_empresa`
--

INSERT INTO `tb_info_empresa` (`idinfo`, `titulo`, `telefone`, `facebook`, `twitter`, `google`, `msg_saudacao`, `ativo`, `ordem`) VALUES
(1, 'Atacadão da Madeira', '(61) 3369-3330 / (61) 3369-1331', 'https://www.facebook.com/', 'https://twitter.com/', 'https://plus.google.com/113650211029829407920', '<p>\r\n	&nbsp;</p>\r\n<div>\r\n	<p style="margin: 0px auto 10px; list-style: none; outline: none; border: none; text-align: justify; font-family: Ubuntu, sans-serif;">\r\n		<strong>A Atacad&atilde;o da Madeira</strong>&nbsp;&eacute; uma empresa s&eacute;ria e pioneira no ramo de madeiras, estamos atuando no mercado de madeiras ha 16 anos. Destacamos pelos produtos de alt&iacute;ssima qualidade e uma pol&iacute;tica de pre&ccedil;os altamente competitiva em rela&ccedil;&atilde;o ao mercado.</p>\r\n	<p style="margin: 0px auto 10px; list-style: none; outline: none; border: none; text-align: justify; font-family: Ubuntu, sans-serif;">\r\n		Procuramos manter sempre em disponibilidade em nosso estoque o material anunciado ao clientes al&eacute;m dos lan&ccedil;amentos feitos por nossos fornecedores. Para assim atender melhor ao cliente nas suas necessidades imediatas.</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>', 'SIM', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `nome`, `senha`, `ativo`, `id_grupologin`, `email`) VALUES
(13, 'Atendimento', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 1, 'atendimento.sites@homewebbrasil.com.br'),
(15, 'Atacadão da Madeira', '4a79f0f6e14e78546d34985ab9f9b00a', 'SIM', 1, 'atacadaodamadeira@hotmail.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(687, 'CADASTRO DE GRUPO Contabilidade', 'INSERT INTO	tb_grupos_logins\r\n					(nome)\r\n					VALUES\r\n					(\'Contabilidade\')', '2011-12-15', '17:25:17', 0),
(688, 'CADASTRO DE GRUPO Buscadores', 'INSERT INTO	tb_grupos_logins\r\n					(nome)\r\n					VALUES\r\n					(\'Buscadores\')', '2011-12-15', '17:26:33', 0),
(689, 'ALTERAÃ‡ÃƒO DE GRUPO 10', 'UPDATE tb_grupos_logins SET nome = \'Buscadores --1 121 654\' WHERE idgrupologin = \'10\'', '2011-12-15', '17:41:11', 0),
(690, 'ALTERAÃ‡ÃƒO DE GRUPO 10', 'UPDATE tb_grupos_logins SET nome = \'Buscadores\' WHERE idgrupologin = \'10\'', '2011-12-15', '17:41:48', 0),
(691, 'EXCLUSÃƒO DO GRUPO COD: 10, NOME: Buscadores', 'DELETE FROM tb_grupos_logins WHERE idgrupologin = \'10\'', '2011-12-15', '17:43:27', 0),
(692, 'EXCLUSÃƒO DO GRUPO COD: 10, NOME: ', 'DELETE FROM tb_grupos_logins WHERE idgrupologin = \'10\'', '2011-12-15', '17:43:28', 0),
(693, 'EXCLUSÃƒO DO GRUPO COD: 8, NOME: Testando', 'DELETE FROM tb_grupos_logins WHERE idgrupologin = \'8\'', '2011-12-15', '17:43:49', 0),
(694, 'DESATIVOU O GRUPO 9', 'UPDATE tb_grupos_logins SET ativo = \'NAO\' WHERE idgrupologin = \'9\'', '2011-12-15', '17:44:49', 0),
(695, 'ATIVOU O GRUPO 9', 'UPDATE tb_grupos_logins SET ativo = \'SIM\' WHERE idgrupologin = \'9\'', '2011-12-15', '17:45:08', 0),
(696, 'DESATIVOU O GRUPO 9', 'UPDATE tb_grupos_logins SET ativo = \'NAO\' WHERE idgrupologin = \'9\'', '2011-12-15', '17:45:12', 0),
(697, 'ATIVOU O GRUPO 9', 'UPDATE tb_grupos_logins SET ativo = \'SIM\' WHERE idgrupologin = \'9\'', '2011-12-15', '17:45:17', 0),
(698, 'CADASTRO DO LOGIN ', 'INSERT INTO	tb_logins\r\n					(nome, senha, email, id_grupologin)\r\n					VALUES\r\n					(\'\', \'d41d8cd98f00b204e9800998ecf8427e\', \'\', \'\')', '2011-12-15', '17:51:02', 0),
(699, 'CADASTRO DO LOGIN Andr?', 'INSERT INTO	tb_logins\r\n					(nome, senha, email, id_grupologin)\r\n					VALUES\r\n					(\'AndrÃ©\', \'e10adc3949ba59abbe56e057f20f883e\', \'marcio@tekan.com.br\', \'1\')', '2011-12-15', '17:51:47', 0),
(700, 'DESATIVOU O LOGIN 11', 'UPDATE tb_logins SET ativo = \'NAO\' WHERE idlogin = \'11\'', '2011-12-15', '17:55:35', 0),
(701, 'ATIVOU O LOGIN 11', 'UPDATE tb_logins SET ativo = \'SIM\' WHERE idlogin = \'11\'', '2011-12-15', '17:55:42', 0),
(702, 'ALTERAÃ‡ÃƒO DO LOGIN 11', 'UPDATE tb_logins SET nome = \'AndrÃ© a\', email = \'marcio@tekan.com.br\', id_grupologin = \'9\' WHERE idlogin = \'11\'', '2011-12-15', '17:57:51', 0),
(703, 'EXCLUSÃƒO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_logins WHERE idlogin = \'10\'', '2011-12-15', '17:58:00', 0),
(704, 'EXCLUSÃƒO DO LOGIN 11, NOME: Andr?, Email: marcio@tekan.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'11\'', '2011-12-15', '17:58:10', 0),
(705, 'CADASTRO DO CLIENTE ', '', '2012-11-07', '22:57:49', 2),
(706, 'CADASTRO DO CLIENTE ', '', '2012-11-07', '22:59:15', 2),
(707, 'CADASTRO DO CLIENTE ', '', '2012-11-07', '23:00:32', 2),
(708, 'CADASTRO DO CLIENTE ', '', '2012-11-07', '23:02:40', 2),
(709, 'CADASTRO DO CLIENTE ', '', '2012-11-07', '23:03:34', 2),
(710, 'CADASTRO DO CLIENTE ', '', '2012-11-07', '23:27:54', 2),
(711, 'CADASTRO DO CLIENTE ', '', '2012-11-07', '23:28:21', 2),
(712, 'CADASTRO DO CLIENTE ', '', '2012-11-08', '21:47:21', 2),
(713, 'DESATIVOU O LOGIN 1', 'UPDATE tb_parceiros SET ativo = \'NAO\' WHERE idparceiro = \'1\'', '2012-11-08', '21:49:15', 2),
(714, 'ATIVOU O LOGIN 1', 'UPDATE tb_parceiros SET ativo = \'SIM\' WHERE idparceiro = \'1\'', '2012-11-08', '21:49:19', 2),
(715, 'EXCLUSÃƒO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'1\'', '2012-11-08', '21:49:24', 2),
(716, 'CADASTRO DO CLIENTE ', '', '2012-11-08', '21:50:08', 2),
(717, 'CADASTRO DO CLIENTE ', '', '2012-11-08', '21:54:36', 2),
(718, 'EXCLUSÃƒO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'3\'', '2012-11-08', '21:55:11', 2),
(719, 'CADASTRO DO CLIENTE ', '', '2012-11-08', '21:59:11', 2),
(720, 'DESATIVOU O LOGIN 1', 'UPDATE tb_servicos SET ativo = \'NAO\' WHERE idservico = \'1\'', '2012-11-08', '22:01:21', 2),
(721, 'ATIVOU O LOGIN 1', 'UPDATE tb_servicos SET ativo = \'SIM\' WHERE idservico = \'1\'', '2012-11-08', '22:01:24', 2),
(722, 'CADASTRO DO CLIENTE ', '', '2012-11-08', '22:26:56', 2),
(723, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-08', '22:27:27', 2),
(724, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-08', '22:28:34', 2),
(725, 'CADASTRO DO CLIENTE ', '', '2012-11-08', '22:30:45', 2),
(726, 'ALTERAÃ‡ÃƒO DO LOGIN 1', 'UPDATE tb_logins SET nome = \'Marcio AndrÃ© da Silva\', email = \'marcio@masmidia.com.br\', id_grupologin = \'1\' WHERE idlogin = \'1\'', '2012-11-08', '23:02:25', 1),
(727, 'CADASTRO DO CLIENTE ', '', '2012-11-09', '21:31:09', 2),
(728, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '21:36:00', 2),
(729, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '21:43:09', 2),
(730, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '21:45:50', 2),
(731, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '21:51:31', 2),
(732, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '21:57:47', 1),
(733, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '21:59:12', 2),
(734, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '22:01:44', 1),
(735, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '22:03:14', 1),
(736, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '22:05:38', 2),
(737, 'CADASTRO DO CLIENTE ', '', '2012-11-09', '22:10:36', 1),
(738, 'CADASTRO DO CLIENTE ', '', '2012-11-09', '22:11:01', 2),
(739, 'CADASTRO DO CLIENTE ', '', '2012-11-09', '22:12:48', 1),
(740, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '22:13:52', 1),
(741, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '22:14:50', 1),
(742, 'CADASTRO DO CLIENTE ', '', '2012-11-09', '23:05:01', 1),
(743, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '23:07:46', 1),
(744, 'CADASTRO DO CLIENTE ', '', '2012-11-09', '23:07:54', 1),
(745, 'CADASTRO DO CLIENTE ', '', '2012-11-09', '23:08:07', 2),
(746, 'CADASTRO DO CLIENTE ', '', '2012-11-09', '23:13:59', 2),
(747, 'CADASTRO DO CLIENTE ', '', '2012-11-09', '23:19:50', 1),
(748, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '23:20:48', 1),
(749, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '23:21:18', 1),
(750, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-09', '23:21:40', 1),
(751, 'CADASTRO DO CLIENTE ', '', '2012-11-09', '23:25:58', 1),
(752, 'EXCLUSÃƒO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'5\'', '2012-11-09', '23:30:00', 2),
(753, 'EXCLUSÃƒO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'4\'', '2012-11-09', '23:30:06', 2),
(754, 'EXCLUSÃƒO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'5\'', '2012-11-09', '23:30:10', 2),
(755, 'EXCLUSÃƒO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'6\'', '2012-11-09', '23:30:14', 2),
(756, 'CADASTRO DO CLIENTE ', '', '2012-11-09', '23:30:50', 1),
(757, 'EXCLUSÃƒO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'6\'', '2012-11-09', '23:30:59', 1),
(758, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '00:07:07', 2),
(759, 'ALTEROU SENHA 1', '', '2012-11-10', '00:12:40', 1),
(760, 'CADASTRO DO CLIENTE ', '', '2012-11-10', '00:13:01', 1),
(761, 'EXCLUSÃƒO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'7\'', '2012-11-10', '00:13:08', 1),
(762, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '00:14:33', 2),
(763, 'CADASTRO DO CLIENTE ', '', '2012-11-10', '00:25:32', 1),
(764, 'CADASTRO DO CLIENTE ', '', '2012-11-10', '00:26:42', 1),
(765, 'CADASTRO DO CLIENTE ', '', '2012-11-10', '00:27:53', 1),
(766, 'CADASTRO DO CLIENTE ', '', '2012-11-10', '00:29:44', 1),
(767, 'EXCLUSÃƒO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'6\'', '2012-11-10', '00:32:57', 1),
(768, 'EXCLUSÃƒO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'5\'', '2012-11-10', '00:33:07', 1),
(769, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '00:37:02', 2),
(770, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '00:37:14', 2),
(771, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '00:37:25', 2),
(772, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '00:37:35', 2),
(773, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '00:39:17', 2),
(774, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '00:40:26', 2),
(775, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '00:43:27', 2),
(776, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '00:43:37', 2),
(777, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '00:43:46', 2),
(778, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '00:43:55', 2),
(779, 'CADASTRO DO LOGIN Amanda', 'INSERT INTO	tb_logins\n					(nome, senha, email, id_grupologin)\n					VALUES\n					(\'Amanda\', \'2412b7b481882ec17ade9e7e9ae6fc37\', \'amanda@homewebbrasil.com.br\', \'1\')', '2012-11-10', '00:51:51', 1),
(780, 'CADASTRO DO LOGIN Angela', 'INSERT INTO	tb_logins\n					(nome, senha, email, id_grupologin)\n					VALUES\n					(\'Angela\', \'2412b7b481882ec17ade9e7e9ae6fc37\', \'atendimento.sites@homewebbrasil.com.br\', \'1\')', '2012-11-10', '00:53:02', 1),
(781, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:01:37', 1),
(782, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:08:42', 1),
(783, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:09:20', 1),
(784, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:09:39', 1),
(785, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:11:31', 1),
(786, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:11:48', 1),
(787, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:13:23', 1),
(788, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:13:40', 1),
(789, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:18:40', 1),
(790, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:19:10', 1),
(791, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:28:33', 1),
(792, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:30:53', 1),
(793, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:32:00', 1),
(794, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:32:56', 1),
(795, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-10', '01:34:24', 1),
(796, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-11', '12:41:33', 2),
(797, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-11', '12:41:59', 2),
(798, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-11', '12:42:14', 2),
(799, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-11', '12:42:33', 2),
(800, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '11:49:33', 13),
(801, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '16:42:18', 13),
(802, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '16:43:52', 13),
(803, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '16:44:05', 13),
(804, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '16:44:47', 13),
(805, 'CADASTRO DO CLIENTE ', '', '2012-11-12', '16:48:36', 13),
(806, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '16:50:43', 13),
(807, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '16:52:59', 13),
(808, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '16:55:48', 13),
(809, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '16:59:09', 13),
(810, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '17:02:20', 13),
(811, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '17:05:41', 13),
(812, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '17:09:22', 13),
(813, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '17:18:39', 13),
(814, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '17:21:51', 13),
(815, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '17:22:17', 13),
(816, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '17:39:10', 13),
(817, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '17:39:55', 13),
(818, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '17:41:02', 13),
(819, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '17:42:03', 13),
(820, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '17:42:28', 13),
(821, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '17:43:07', 13),
(822, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '17:43:44', 13),
(823, 'CADASTRO DO CLIENTE ', '', '2012-11-12', '17:46:54', 13),
(824, 'CADASTRO DO CLIENTE ', '', '2012-11-12', '17:47:15', 13),
(825, 'CADASTRO DO CLIENTE ', '', '2012-11-12', '17:48:44', 13),
(826, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '17:59:44', 13),
(827, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:02:48', 13),
(828, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:05:09', 13),
(829, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:05:54', 13),
(830, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:07:56', 13),
(831, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:09:10', 13),
(832, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:10:43', 13),
(833, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:11:58', 13),
(834, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:12:38', 13),
(835, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:13:07', 13),
(836, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:13:31', 13),
(837, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:13:54', 13),
(838, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:14:41', 13),
(839, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:15:11', 13),
(840, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:15:50', 13),
(841, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:16:28', 13),
(842, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:18:00', 13),
(843, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:22:02', 13),
(844, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:23:52', 13),
(845, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:25:19', 13),
(846, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:25:56', 13),
(847, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:26:24', 13),
(848, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:28:27', 13),
(849, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:29:15', 13),
(850, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:30:16', 13),
(851, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:31:10', 13),
(852, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:32:08', 13),
(853, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:32:40', 13),
(854, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:33:05', 13),
(855, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '18:33:26', 13),
(856, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '22:24:45', 1),
(857, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '22:46:38', 2),
(858, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '22:46:43', 2),
(859, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '22:46:54', 2),
(860, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '22:47:04', 2),
(861, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '22:47:13', 2),
(862, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '22:48:55', 2),
(863, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '22:49:04', 2),
(864, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '22:49:13', 2),
(865, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '22:49:23', 2),
(866, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '22:50:00', 2),
(867, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '23:02:46', 2),
(868, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-12', '23:06:34', 1),
(869, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-13', '12:41:50', 13),
(870, 'CADASTRO DO CLIENTE ', '', '2012-11-13', '17:14:21', 13),
(871, 'CADASTRO DO CLIENTE ', '', '2012-11-13', '17:19:14', 13),
(872, 'DESATIVOU O LOGIN 3', 'UPDATE tb_servicos SET ativo = \'NAO\' WHERE idservico = \'3\'', '2012-11-13', '17:20:50', 13),
(873, 'DESATIVOU O LOGIN 4', 'UPDATE tb_servicos SET ativo = \'NAO\' WHERE idservico = \'4\'', '2012-11-13', '17:20:58', 13),
(874, 'DESATIVOU O LOGIN 7', 'UPDATE tb_servicos SET ativo = \'NAO\' WHERE idservico = \'7\'', '2012-11-13', '17:21:09', 13),
(875, 'CADASTRO DO CLIENTE ', '', '2012-11-13', '17:30:20', 13),
(876, 'CADASTRO DO CLIENTE ', '', '2012-11-13', '17:32:49', 13),
(877, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-13', '17:33:22', 13),
(878, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-13', '17:34:08', 13),
(879, 'DESATIVOU O LOGIN 1', 'UPDATE tb_servicos SET ativo = \'NAO\' WHERE idservico = \'1\'', '2012-11-13', '17:34:30', 13),
(880, 'DESATIVOU O LOGIN 2', 'UPDATE tb_servicos SET ativo = \'NAO\' WHERE idservico = \'2\'', '2012-11-13', '17:34:38', 13),
(881, 'CADASTRO DO CLIENTE ', '', '2012-11-13', '17:37:00', 13),
(882, 'CADASTRO DO CLIENTE ', '', '2012-11-13', '18:10:23', 13),
(883, 'CADASTRO DO CLIENTE ', '', '2012-11-13', '18:14:56', 13),
(884, 'CADASTRO DO CLIENTE ', '', '2012-11-13', '18:21:45', 13),
(885, 'CADASTRO DO CLIENTE ', '', '2012-11-13', '20:33:56', 13),
(886, 'DESATIVOU O LOGIN 14', 'UPDATE tb_servicos SET ativo = \'NAO\' WHERE idservico = \'14\'', '2012-11-13', '20:35:26', 13),
(887, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-14', '14:21:23', 13),
(888, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-14', '14:30:48', 13),
(889, 'ATIVOU O LOGIN 1', 'UPDATE tb_servicos SET ativo = \'SIM\' WHERE idservico = \'1\'', '2012-11-14', '14:41:29', 13),
(890, 'EXCLUSÃƒO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'2\'', '2012-11-14', '14:58:22', 13),
(891, 'DESATIVOU O LOGIN 1', 'UPDATE tb_servicos SET ativo = \'NAO\' WHERE idservico = \'1\'', '2012-11-14', '15:08:21', 13),
(892, 'ATIVOU O LOGIN 1', 'UPDATE tb_servicos SET ativo = \'SIM\' WHERE idservico = \'1\'', '2012-11-20', '11:29:11', 13),
(893, 'ATIVOU O LOGIN 3', 'UPDATE tb_servicos SET ativo = \'SIM\' WHERE idservico = \'3\'', '2012-11-20', '11:29:20', 13),
(894, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '11:36:58', 13),
(895, 'EXCLUSÃƒO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'1\'', '2012-11-20', '11:43:46', 13),
(896, 'ATIVOU O LOGIN 4', 'UPDATE tb_servicos SET ativo = \'SIM\' WHERE idservico = \'4\'', '2012-11-20', '12:00:39', 13),
(897, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '12:01:37', 13),
(898, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '12:02:31', 13),
(899, 'ATIVOU O LOGIN 7', 'UPDATE tb_servicos SET ativo = \'SIM\' WHERE idservico = \'7\'', '2012-11-20', '12:07:59', 13),
(900, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '12:09:29', 13),
(901, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '15:14:09', 13),
(902, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '15:15:32', 13),
(903, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '15:16:29', 13),
(904, 'ATIVOU O LOGIN 14', 'UPDATE tb_servicos SET ativo = \'SIM\' WHERE idservico = \'14\'', '2012-11-20', '15:23:31', 13),
(905, 'EXCLUSÃƒO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'14\'', '2012-11-20', '15:29:49', 13),
(906, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '15:35:50', 13),
(907, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '15:36:52', 13),
(908, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '15:37:30', 13),
(909, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '15:40:09', 13),
(910, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '15:41:30', 13),
(911, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '15:42:06', 13),
(912, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '15:49:47', 13),
(913, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '15:51:06', 13),
(914, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '15:52:43', 13),
(915, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '15:58:27', 13),
(916, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '16:01:00', 13),
(917, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '16:05:56', 13),
(918, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '16:09:03', 13),
(919, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '16:10:31', 13),
(920, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '16:10:53', 13),
(921, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '16:11:13', 13),
(922, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '16:12:29', 13),
(923, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '16:15:43', 13),
(924, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '16:21:54', 13),
(925, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '16:55:31', 13),
(926, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '16:55:50', 13),
(927, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '17:00:42', 13),
(928, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '17:01:35', 13),
(929, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '17:02:00', 13),
(930, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '17:03:22', 13),
(931, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '17:07:33', 13),
(932, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '17:08:04', 13),
(933, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '17:08:31', 13),
(934, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '17:08:54', 13),
(935, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '17:09:31', 13),
(936, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '17:12:04', 13),
(937, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '17:20:10', 13),
(938, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '17:28:28', 13),
(939, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '17:41:09', 13),
(940, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '17:57:21', 13),
(941, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '18:00:04', 13),
(942, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '18:05:02', 13),
(943, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '18:08:20', 13),
(944, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '18:08:51', 13),
(945, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '18:09:48', 13),
(946, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '18:10:24', 13),
(947, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-20', '18:12:19', 13),
(948, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:05:41', 13),
(949, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:07:27', 13),
(950, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:07:54', 13),
(951, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:08:29', 13),
(952, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:08:56', 13),
(953, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:12:15', 13),
(954, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:12:57', 13),
(955, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:13:28', 13),
(956, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:14:17', 13),
(957, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:15:11', 13),
(958, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:15:43', 13),
(959, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:17:06', 13),
(960, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:17:31', 13),
(961, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:18:49', 13),
(962, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:21:00', 13),
(963, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:23:14', 13),
(964, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:23:41', 13),
(965, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:27:28', 13),
(966, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:45:08', 13),
(967, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:47:01', 13),
(968, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '18:49:07', 13),
(969, 'CADASTRO DO CLIENTE ', '', '2012-11-21', '18:51:41', 13),
(970, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '19:20:07', 13),
(971, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '19:24:47', 13),
(972, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '19:27:17', 13),
(973, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '19:28:04', 13),
(974, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '19:28:26', 13),
(975, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '19:28:57', 13),
(976, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '19:30:22', 13),
(977, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '19:30:41', 13),
(978, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '19:31:10', 13),
(979, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '19:32:10', 13),
(980, 'CADASTRO DO CLIENTE ', '', '2012-11-21', '22:07:45', 2),
(981, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '22:11:04', 1),
(982, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '22:11:54', 1),
(983, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '22:14:50', 1),
(984, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '22:15:49', 1),
(985, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '22:22:49', 2),
(986, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '22:24:05', 2),
(987, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '22:25:44', 2),
(988, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '22:36:55', 1),
(989, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '22:37:27', 1),
(990, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '22:39:23', 1),
(991, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '23:06:46', 1),
(992, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-21', '23:07:42', 1),
(993, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '11:58:26', 13),
(994, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '11:59:08', 13),
(995, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '12:13:16', 13),
(996, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '12:15:20', 13),
(997, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '12:16:56', 13),
(998, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '12:20:46', 13),
(999, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '12:21:46', 13),
(1000, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '12:22:52', 13),
(1001, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:18:06', 13),
(1002, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:19:41', 13),
(1003, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:20:35', 13),
(1004, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:21:45', 13),
(1005, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:22:16', 13),
(1006, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:22:37', 13),
(1007, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:24:54', 13),
(1008, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:26:49', 13),
(1009, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:46:14', 13),
(1010, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:47:19', 13),
(1011, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:49:02', 13),
(1012, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:49:49', 13),
(1013, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:51:20', 13),
(1014, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:52:19', 13),
(1015, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:53:20', 13),
(1016, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:54:37', 13),
(1017, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:55:42', 13),
(1018, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-26', '15:56:20', 13),
(1019, 'CADASTRO DO CLIENTE ', '', '2012-11-30', '19:53:24', 3),
(1020, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-30', '20:04:50', 3),
(1021, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-30', '20:06:42', 3),
(1022, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-11-30', '20:08:04', 3),
(1023, 'DESATIVOU O LOGIN 18', 'UPDATE tb_servicos SET ativo = \'NAO\' WHERE idservico = \'18\'', '2012-11-30', '20:15:48', 3),
(1024, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-01', '18:35:18', 13),
(1025, 'ATIVOU O LOGIN 18', 'UPDATE tb_servicos SET ativo = \'SIM\' WHERE idservico = \'18\'', '2012-12-01', '18:40:24', 13),
(1026, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-01', '18:45:07', 13),
(1027, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-01', '18:53:16', 13),
(1028, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-01', '19:05:47', 13),
(1029, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-01', '19:22:11', 13),
(1030, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-03', '11:41:52', 13),
(1031, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-03', '11:43:28', 13),
(1032, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-04', '10:39:56', 3),
(1033, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-04', '10:56:22', 3),
(1034, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-04', '10:56:57', 3),
(1035, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-04', '10:57:23', 3),
(1036, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-04', '10:57:50', 3),
(1037, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-04', '10:58:16', 3),
(1038, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-04', '10:59:17', 3),
(1039, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-05', '12:51:51', 1),
(1040, 'ALTERAÃ‡ÃƒO DO CLIENTE ', '', '2012-12-05', '12:53:35', 1),
(1041, 'CADASTRO DO CLIENTE ', '', '2013-02-16', '11:16:27', 2),
(1042, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '11:16:59', 2),
(1043, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '11:32:14', 2),
(1044, 'CADASTRO DO CLIENTE ', '', '2013-02-16', '11:38:49', 2),
(1045, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '12:04:07', 2),
(1046, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '12:04:39', 2),
(1047, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '12:05:10', 2),
(1048, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '12:06:57', 2),
(1049, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '12:08:05', 2),
(1050, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '12:10:39', 2),
(1051, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '12:20:58', 2),
(1052, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '12:21:29', 2),
(1053, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '12:24:16', 2),
(1054, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '12:24:30', 2),
(1055, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '13:10:24', 2),
(1056, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '13:51:10', 2),
(1057, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '13:51:43', 2),
(1058, 'CADASTRO DO CLIENTE ', '', '2013-02-16', '15:33:37', 2),
(1059, 'CADASTRO DO CLIENTE ', '', '2013-02-16', '15:34:08', 2),
(1060, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-16', '15:58:33', 2),
(1061, 'CADASTRO DO CLIENTE ', '', '2013-02-16', '16:23:41', 2),
(1062, 'CADASTRO DO CLIENTE ', '', '2013-02-18', '20:14:23', 2),
(1063, 'CADASTRO DO CLIENTE ', '', '2013-02-18', '20:15:03', 2),
(1064, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-18', '20:39:52', 2),
(1065, 'ALTERAÇÃO DO LOGIN 1', 'UPDATE tb_logins SET nome = \'Marcio André da Silva\', email = \'marcio@masmidia.com.br\', id_grupologin = \'1\' WHERE idlogin = \'1\'', '2013-02-18', '22:32:57', 1),
(1066, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:18:56', 13),
(1067, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:20:03', 13),
(1068, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:21:05', 13),
(1069, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:22:07', 13),
(1070, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:22:55', 13),
(1071, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:25:51', 13),
(1072, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:26:36', 13),
(1073, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:29:02', 13),
(1074, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:29:52', 13),
(1075, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:30:56', 13),
(1076, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:31:46', 13),
(1077, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2013-02-28', '11:32:24', 13),
(1078, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2013-02-28', '11:32:31', 13),
(1079, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:34:01', 13),
(1080, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:35:06', 13),
(1081, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:36:45', 13),
(1082, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:37:57', 13),
(1083, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:38:39', 13),
(1084, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:39:20', 13),
(1085, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:40:09', 13),
(1086, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:40:09', 13),
(1087, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:41:09', 13),
(1088, 'DESATIVOU O LOGIN 1', 'UPDATE tb_servicos SET ativo = \'NAO\' WHERE idservico = \'1\'', '2013-02-28', '11:47:00', 13),
(1089, 'CADASTRO DO CLIENTE ', '', '2013-02-28', '11:49:44', 13),
(1090, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'8\'', '2013-02-28', '11:54:49', 13),
(1091, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '11:56:37', 13),
(1092, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '11:59:38', 13),
(1093, 'DESATIVOU O LOGIN 2', 'UPDATE tb_enderecos SET ativo = \'NAO\' WHERE idendereco = \'2\'', '2013-02-28', '12:00:24', 13),
(1094, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '12:01:54', 13),
(1095, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '12:02:32', 13),
(1096, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '12:03:01', 13),
(1097, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '12:03:25', 13),
(1098, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '12:03:57', 13),
(1099, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '15:56:34', 13),
(1100, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'4\'', '2013-02-28', '15:58:24', 13),
(1101, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '15:58:41', 13),
(1102, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '16:08:40', 13),
(1103, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '16:20:41', 13),
(1104, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '16:21:42', 13),
(1105, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '16:24:17', 13),
(1106, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '16:42:27', 13),
(1107, 'ALTERAÇÃO DO CLIENTE ', '', '2013-02-28', '16:43:59', 13),
(1108, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'1\'', '2013-03-01', '16:57:52', 13),
(1109, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'2\'', '2013-03-01', '16:57:57', 13),
(1110, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '16:58:26', 13),
(1111, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '16:58:58', 13),
(1112, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '16:59:28', 13),
(1113, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:00:08', 13),
(1114, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:00:53', 13),
(1115, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:01:18', 13),
(1116, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:01:51', 13),
(1117, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:17:36', 13),
(1118, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:18:19', 13),
(1119, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:20:49', 13),
(1120, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:21:59', 13),
(1121, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:23:03', 13),
(1122, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:26:56', 13),
(1123, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:32:16', 13),
(1124, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:33:39', 13),
(1125, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:34:40', 13),
(1126, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:36:24', 13),
(1127, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:42:34', 13),
(1128, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-01', '17:50:59', 13),
(1129, 'DESATIVOU O LOGIN 9', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'9\'', '2013-03-04', '11:24:22', 13),
(1130, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-04', '11:24:55', 13),
(1131, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-04', '11:29:14', 13),
(1132, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-04', '11:29:46', 13),
(1133, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-04', '11:30:19', 13),
(1134, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-04', '11:30:46', 13),
(1135, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-04', '12:02:33', 13),
(1136, 'DESATIVOU O LOGIN 12', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'12\'', '2013-03-04', '12:06:59', 13),
(1137, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'9\'', '2013-03-04', '12:08:03', 13),
(1138, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'12\'', '2013-03-04', '12:19:17', 13),
(1139, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-04', '12:19:36', 13),
(1140, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-04', '15:05:03', 13),
(1141, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-04', '15:12:42', 13),
(1142, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-04', '15:14:43', 13),
(1143, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-04', '15:21:26', 13),
(1144, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-04', '15:25:12', 13),
(1145, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'1\'', '2013-03-04', '15:33:41', 13),
(1146, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-04', '15:38:07', 13),
(1147, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-04', '15:38:44', 13),
(1148, 'CADASTRO DO CLIENTE ', '', '2013-03-04', '15:47:19', 13),
(1149, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'1\'', '2013-03-04', '15:48:32', 13),
(1150, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'2\'', '2013-03-04', '15:49:34', 13),
(1151, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'3\'', '2013-03-04', '15:49:52', 13),
(1152, 'CADASTRO DO CLIENTE ', '', '2013-03-04', '17:14:40', 13),
(1153, 'CADASTRO DO CLIENTE ', '', '2013-03-04', '17:25:42', 13),
(1154, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '10:53:01', 13),
(1155, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '10:54:40', 13),
(1156, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '10:56:55', 13),
(1157, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '10:57:56', 13),
(1158, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '10:58:49', 13),
(1159, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '10:59:03', 13),
(1160, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '10:59:14', 13),
(1161, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '10:59:26', 13),
(1162, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '10:59:41', 13),
(1163, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:00:15', 13),
(1164, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:00:27', 13),
(1165, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:00:39', 13),
(1166, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:00:52', 13),
(1167, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:03:02', 13),
(1168, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:04:13', 13),
(1169, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:06:13', 13),
(1170, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:07:12', 13),
(1171, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:09:04', 13),
(1172, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:09:48', 13),
(1173, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:10:11', 13),
(1174, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:10:32', 13),
(1175, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:11:15', 13),
(1176, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:11:56', 13),
(1177, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:12:42', 13),
(1178, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:13:14', 13),
(1179, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-10', '11:13:50', 13),
(1180, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-11', '20:22:01', 2),
(1181, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-11', '20:22:19', 2),
(1182, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-11', '20:26:25', 2),
(1183, 'CADASTRO DO CLIENTE ', '', '2013-03-13', '21:32:04', 2),
(1184, 'CADASTRO DO CLIENTE ', '', '2013-03-13', '21:32:21', 2),
(1185, 'CADASTRO DO CLIENTE ', '', '2013-03-13', '21:32:49', 2),
(1186, 'DESATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'1\'', '2013-03-13', '21:33:07', 2),
(1187, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-13', '21:33:45', 2),
(1188, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'4\'', '2013-03-13', '21:39:45', 2),
(1189, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-13', '21:39:55', 2),
(1190, 'CADASTRO DO CLIENTE ', '', '2013-03-13', '21:40:08', 2),
(1191, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-13', '21:43:57', 2),
(1192, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-13', '21:45:25', 2),
(1193, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-13', '21:47:27', 2),
(1194, 'ALTERAÇÃO DO CLIENTE ', '', '2013-03-13', '21:51:40', 2),
(1195, 'ATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'1\'', '2013-03-14', '10:29:31', 1),
(1196, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2013-03-14', '10:31:17', 1),
(1197, 'ALTERAÇÃO DO CLIENTE ', '', '2013-04-01', '17:28:41', 13),
(1198, 'ALTERAÇÃO DO CLIENTE ', '', '2013-04-01', '17:30:19', 13),
(1199, 'ALTERAÇÃO DO CLIENTE ', '', '2013-04-01', '17:31:23', 13),
(1200, 'ALTERAÇÃO DO CLIENTE ', '', '2013-04-01', '17:33:09', 13),
(1201, 'ALTERAÇÃO DO CLIENTE ', '', '2013-04-01', '17:34:17', 13),
(1202, 'ALTERAÇÃO DO CLIENTE ', '', '2013-04-01', '17:35:34', 13),
(1203, 'ALTERAÇÃO DO CLIENTE ', '', '2013-04-01', '17:43:09', 13),
(1204, 'ALTERAÇÃO DO CLIENTE ', '', '2013-04-01', '17:43:32', 13),
(1205, 'ALTERAÇÃO DO CLIENTE ', '', '2013-04-01', '17:44:45', 13),
(1206, 'ALTERAÇÃO DO CLIENTE ', '', '2013-04-01', '17:45:08', 13),
(1207, 'ALTERAÇÃO DO CLIENTE ', '', '2013-04-01', '17:45:26', 13),
(1208, 'ALTERAÇÃO DO CLIENTE ', '', '2013-04-01', '17:46:12', 13),
(1209, 'CADASTRO DO LOGIN Atacad?o da Madeira', 'INSERT INTO	tb_logins\n					(nome, senha, email, id_grupologin)\n					VALUES\n					(\'Atacadão da Madeira\', \'e10adc3949ba59abbe56e057f20f883e\', \'atacadaodamadeira@hotmail.com\', \'1\')', '2013-04-08', '12:04:13', 13),
(1210, 'EXCLUSÃO DO LOGIN 4, NOME: Mas Midia, Email: contato@masmdiaia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'4\'', '2013-06-06', '16:32:00', 13),
(1211, 'EXCLUSÃO DO LOGIN 3, NOME: Kayo Leandro, Email: kayo@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'3\'', '2013-06-06', '16:32:11', 13),
(1212, 'EXCLUSÃO DO LOGIN 12, NOME: Amanda, Email: amanda@homewebbrasil.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'12\'', '2013-06-06', '16:32:23', 13),
(1213, 'EXCLUSÃO DO LOGIN 2, NOME: David Leandro dos Santos, Email: david@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'2\'', '2013-06-06', '16:32:34', 13),
(1214, 'EXCLUSÃO DO LOGIN 1, NOME: Marcio Andr? da Silva, Email: marcio@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'1\'', '2013-06-06', '16:32:43', 13),
(1215, 'EXCLUSÃO DO LOGIN 14, NOME: Atacad?o da Madeira, Email: atacadaodamadeira@hotmail.com', 'DELETE FROM tb_logins WHERE idlogin = \'14\'', '2013-06-06', '16:32:57', 13),
(1216, 'CADASTRO DO LOGIN Atacad?o da Madeira', 'INSERT INTO	tb_logins\n					(nome, senha, email, id_grupologin)\n					VALUES\n					(\'Atacadão da Madeira\', \'4a79f0f6e14e78546d34985ab9f9b00a\', \'atacadaodamadeira@hotmail.com\', \'\')', '2013-06-06', '16:33:58', 13),
(1217, 'ALTERAÇÃO DO LOGIN 13', 'UPDATE tb_logins SET nome = \'Atendimento\', email = \'atendimento.sites@homewebbrasil.com.br\', id_grupologin = \'1\' WHERE idlogin = \'13\'', '2013-06-06', '16:34:27', 13),
(1218, 'ALTERAÇÃO DO LOGIN 15', 'UPDATE tb_logins SET nome = \'Atacad?o da Madeira\', email = \'atacadaodamadeira@hotmail.com\', id_grupologin = \'1\' WHERE idlogin = \'15\'', '2013-06-06', '16:34:40', 13),
(1219, 'ALTERAÇÃO DO LOGIN 15', 'UPDATE tb_logins SET nome = \'Atacadão da Madeira\', email = \'atacadaodamadeira@hotmail.com\', id_grupologin = \'1\' WHERE idlogin = \'15\'', '2013-06-06', '16:34:52', 13),
(1220, 'ALTERAÇÃO DO CLIENTE ', '', '2013-06-06', '16:58:05', 15),
(1221, 'ALTERAÇÃO DO CLIENTE ', '', '2013-06-06', '16:58:20', 15),
(1222, 'ALTERAÇÃO DO CLIENTE ', '', '2013-06-06', '16:59:19', 15),
(1223, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '17:57:05', 13),
(1224, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '18:09:13', 13),
(1225, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '18:09:36', 13),
(1226, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '18:14:14', 13),
(1227, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '18:25:33', 13),
(1228, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '18:25:49', 13),
(1229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '18:35:31', 13),
(1230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '18:36:03', 13),
(1231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '18:38:03', 13),
(1232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '18:39:09', 13),
(1233, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'4\'', '2016-06-17', '18:50:26', 13),
(1234, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '18:57:13', 13),
(1235, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '18:59:53', 13),
(1236, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '19:11:32', 13),
(1237, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '19:21:37', 13),
(1238, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '19:27:45', 13),
(1239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '19:32:54', 13),
(1240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '19:40:05', 13),
(1241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '19:48:15', 13),
(1242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '19:53:54', 13),
(1243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '19:55:18', 13),
(1244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '20:01:45', 13),
(1245, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '20:08:29', 13),
(1246, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '20:10:56', 13),
(1247, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '20:11:56', 13),
(1248, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '20:12:44', 13),
(1249, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '20:16:27', 13),
(1250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '20:17:10', 13),
(1251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '20:17:42', 13),
(1252, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '20:19:12', 13),
(1253, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '20:33:07', 13),
(1254, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '20:40:14', 13),
(1255, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'2\'', '2016-06-17', '20:42:23', 13),
(1256, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'6\'', '2016-06-17', '20:42:29', 13),
(1257, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'5\'', '2016-06-17', '20:42:47', 13),
(1258, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'9\'', '2016-06-17', '20:42:52', 13),
(1259, 'CADASTRO DO CLIENTE ', '', '2016-06-17', '20:46:15', 13),
(1260, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'10\'', '2016-06-17', '20:46:59', 13),
(1261, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '20:53:12', 13),
(1262, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '20:57:33', 13),
(1263, 'CADASTRO DO CLIENTE ', '', '2016-06-17', '21:24:22', 13),
(1264, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '21:25:27', 13),
(1265, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '21:51:46', 13),
(1266, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '22:15:34', 13),
(1267, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-17', '22:16:05', 13),
(1268, 'CADASTRO DO CLIENTE ', '', '2016-06-17', '22:16:41', 13),
(1269, 'CADASTRO DO CLIENTE ', '', '2016-06-17', '22:19:13', 13),
(1270, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'16\'', '2016-06-17', '22:20:38', 13),
(1271, 'DESATIVOU O LOGIN 7', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'7\'', '2016-06-17', '22:26:41', 13),
(1272, 'DESATIVOU O LOGIN 13', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'13\'', '2016-06-17', '22:27:09', 13),
(1273, 'CADASTRO DO CLIENTE ', '', '2016-06-17', '22:32:04', 13),
(1274, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-18', '17:07:41', 13),
(1275, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '10:37:26', 13),
(1276, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '10:38:59', 13),
(1277, 'ATIVOU O LOGIN 2', 'UPDATE tb_enderecos SET ativo = \'SIM\' WHERE idendereco = \'2\'', '2016-06-29', '10:39:06', 13),
(1278, 'DESATIVOU O LOGIN 2', 'UPDATE tb_enderecos SET ativo = \'NAO\' WHERE idendereco = \'2\'', '2016-06-29', '10:39:31', 13),
(1279, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '10:41:08', 13),
(1280, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '10:41:39', 13),
(1281, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-06-29', '10:45:59', 13),
(1282, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-06-29', '10:46:49', 13),
(1283, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '10:47:13', 13),
(1284, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '10:47:18', 13),
(1285, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '10:47:23', 13),
(1286, 'CADASTRO DO CLIENTE ', '', '2016-07-01', '11:47:13', 13),
(1287, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'7\'', '2016-07-01', '11:48:02', 13),
(1288, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '11:56:30', 13),
(1289, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '12:11:03', 13),
(1290, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '12:21:06', 13),
(1291, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '12:21:49', 13),
(1292, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '12:22:36', 13),
(1293, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '12:23:09', 13),
(1294, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '12:25:18', 13),
(1295, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '12:29:57', 13),
(1296, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '12:35:24', 13),
(1297, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '12:51:47', 13),
(1298, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:20:56', 13),
(1299, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-19', '18:32:48', 13),
(1300, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-19', '18:34:37', 13),
(1301, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-19', '18:35:50', 13),
(1302, 'ATIVOU O LOGIN 2', 'UPDATE tb_enderecos SET ativo = \'SIM\' WHERE idendereco = \'2\'', '2016-08-19', '18:36:10', 13),
(1303, 'DESATIVOU O LOGIN 2', 'UPDATE tb_enderecos SET ativo = \'NAO\' WHERE idendereco = \'2\'', '2016-08-19', '18:36:58', 13);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_modulos_paginas`
--

CREATE TABLE `tb_modulos_paginas` (
  `idmodulopagina` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `icone_imagem` varchar(45) DEFAULT NULL,
  `ativo` char(3) NOT NULL DEFAULT 'SIM'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_modulos_paginas`
--

INSERT INTO `tb_modulos_paginas` (`idmodulopagina`, `nome`, `icone_imagem`, `ativo`) VALUES
(1, 'Grupos', NULL, 'SIM'),
(2, 'Logins', NULL, 'SIM'),
(28, 'Empresa', NULL, 'SIM'),
(29, 'Banners', NULL, 'SIM'),
(30, 'Produtos', NULL, 'SIM'),
(31, 'Serviços', NULL, 'SIM'),
(32, 'Endereços', NULL, 'SIM'),
(33, 'Informações Empresa', NULL, 'SIM'),
(34, 'Dicas', NULL, 'SIM');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_paginas`
--

CREATE TABLE `tb_paginas` (
  `idpagina` int(11) NOT NULL,
  `pagina` varchar(255) DEFAULT NULL,
  `imagem` varchar(45) DEFAULT NULL,
  `label` varchar(45) DEFAULT NULL,
  `exibir_menu` varchar(3) DEFAULT 'SIM',
  `descricao` varchar(255) DEFAULT NULL,
  `id_modulopagina` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_paginas`
--

INSERT INTO `tb_paginas` (`idpagina`, `pagina`, `imagem`, `label`, `exibir_menu`, `descricao`, `id_modulopagina`) VALUES
(148, '/admin/grupo/altera.php', NULL, 'Alterar', 'NAO', NULL, 1),
(149, '/admin/grupo/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 1),
(150, '/admin/grupo/exclui.php', NULL, 'Excluir', 'NAO', NULL, 1),
(151, '/admin/grupo/lista.php', NULL, 'Listar', 'SIM', NULL, 1),
(152, '/admin/grupo/permissoes.php', NULL, 'PermissÃµes', 'NAO', NULL, 1),
(153, '/admin/grupo/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 1),
(154, '/admin/login/altera.php', NULL, 'Alterar', 'NAO', NULL, 2),
(155, '/admin/login/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 2),
(156, '/admin/login/exclui.php', NULL, 'Excluir', 'NAO', NULL, 2),
(157, '/admin/login/lista.php', NULL, 'Listar', 'SIM', NULL, 2),
(158, '/admin/login/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 2),
(159, '/admin/empresa/altera.php', NULL, 'Alterar', 'NAO', NULL, 28),
(160, '/admin/empresa/cadastra.php', NULL, 'Cadastrar', 'NAO', NULL, 28),
(161, '/admin/empresa/exclui.php', NULL, 'Excluir', 'NAO', NULL, 28),
(162, '/admin/empresa/lista.php', NULL, 'Listar', 'SIM', NULL, 28),
(163, '/admin/empresa/envia_imagens.php', NULL, 'Envia imagens', 'NAO', NULL, 28),
(164, '/admin/empresa/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 28),
(165, '/admin/banners/altera.php', NULL, 'Alterar', 'NAO', NULL, 29),
(166, '/admin/banners/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 29),
(167, '/admin/banners/exclui.php', NULL, 'Excluir', 'NAO', NULL, 29),
(168, '/admin/banners/lista.php', NULL, 'Listar', 'SIM', NULL, 29),
(169, '/admin/banners/envia_imagens.php', NULL, 'Envia imagens', 'NAO', NULL, 29),
(170, '/admin/banners/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 29),
(171, '/admin/produtos/altera.php', NULL, 'Alterar', 'NAO', NULL, 30),
(172, '/admin/produtos/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 30),
(173, '/admin/produtos/exclui.php', NULL, 'Excluir', 'NAO', NULL, 30),
(174, '/admin/produtos/lista.php', NULL, 'Listar', 'SIM', NULL, 30),
(175, '/admin/produtos/envia_imagens.php', NULL, 'Envia imagens', 'NAO', NULL, 30),
(176, '/admin/produtos/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 30),
(177, '/admin/servicos/altera.php', NULL, 'Alterar', 'NAO', NULL, 31),
(178, '/admin/servicos/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 31),
(179, '/admin/servicos/exclui.php', NULL, 'Excluir', 'NAO', NULL, 31),
(180, '/admin/servicos/lista.php', NULL, 'Listar', 'SIM', NULL, 31),
(181, '/admin/servicos/envia_imagens.php', NULL, 'Envia imagens', 'NAO', NULL, 31),
(182, '/admin/servicos/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 31),
(183, '/admin/enderecos/altera.php', NULL, 'Alterar', 'NAO', NULL, 32),
(184, '/admin/enderecos/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 32),
(185, '/admin/enderecos/exclui.php', NULL, 'Excluir', 'NAO', NULL, 32),
(186, '/admin/enderecos/lista.php', NULL, 'Listar', 'SIM', NULL, 32),
(187, '/admin/enderecos/envia_imagens.php', NULL, 'Envia imagens', 'NAO', NULL, 32),
(188, '/admin/enderecos/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 32),
(189, '/admin/info-empresa/altera.php', NULL, 'Alterar', 'NAO', NULL, 33),
(190, '/admin/info-empresa/cadastra.php', NULL, 'Cadastrar', 'NAO', NULL, 33),
(191, '/admin/info-empresa/exclui.php', NULL, 'Excluir', 'NAO', NULL, 33),
(192, '/admin/info-empresa/lista.php', NULL, 'Listar', 'SIM', NULL, 33),
(193, '/admin/info-empresa/envia_imagens.php', NULL, 'Envia imagens', 'NAO', NULL, 33),
(194, '/admin/info-empresa/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 33),
(195, '/admin/dicas/altera.php', NULL, 'Alterar', 'NAO', NULL, 34),
(196, '/admin/dicas/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 34),
(197, '/admin/dicas/exclui.php', NULL, 'Excluir', 'NAO', NULL, 34),
(198, '/admin/dicas/lista.php', NULL, 'Listar', 'SIM', NULL, 34),
(199, '/admin/dicas/envia_imagens.php', NULL, 'Envia imagens', 'NAO', NULL, 34),
(200, '/admin/dicas/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 34);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE `tb_produtos` (
  `idproduto` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `ativo`, `ordem`, `keywords`, `description`) VALUES
(3, 'Madeiras', '1806201605078725541354.jpg', '<p style="text-align: justify;">\r\n	A <strong>Atacad&atilde;o da Madeira</strong> disponibiliza para nossos clientes as mais diversas madeiras, seja para constru&ccedil;&atilde;o ou para m&oacute;veis, legalizadas e com certificado de extra&ccedil;&atilde;o. Confira alguns dos tipos de madeiras que n&oacute;s comercializamos:</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	<strong>Peroba vermelha,</strong></p>\r\n<p style="text-align: justify;">\r\n	<strong>Cedrinho,</strong></p>\r\n<p style="text-align: justify;">\r\n	<strong>Pinus,</strong></p>\r\n<p style="text-align: justify;">\r\n	<strong>Angelim vermelho,</strong></p>\r\n<p style="text-align: justify;">\r\n	<strong>Tuturub&aacute;.</strong></p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Para maiores informa&ccedil;&otilde;es entre em contato conosco e solicite um or&ccedil;amento.</p>', 'SIM', 0, '', ''),
(5, 'Escoras Eucaliptos', '1706201608339834609342.jpg', '<p>\r\n	A <strong>Atacad&atilde;o da Madeira</strong> disponibiliza em nosso estoque escoras de eucalipto com casca, para utiliza&ccedil;&atilde;o em obras.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Produto ecologicamente correto &ndash; madeira de reflorestamento.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para maiores informa&ccedil;&otilde;es entre em contato conosco e solicite um or&ccedil;amento.</p>', 'SIM', 0, '', ''),
(6, 'Compensado', '1706201606573393598314.jpg', '<p style="text-align: justify;">\r\n	A <strong>Atacad&atilde;o da Madeira</strong> atua no com&eacute;rcio e distribui&ccedil;&atilde;o de compensados indicados para estrutura de m&oacute;veis, divis&oacute;rias, pain&eacute;s, artesanato, brinquedos e pisos para ambientes &uacute;midos como banheiro, laboratorios, interior de barcos, cascos de barcos.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Para maiores informa&ccedil;&otilde;es entre em contato conosco e solicite um or&ccedil;amento.</p>', 'SIM', 0, '', ''),
(7, 'Telhas', '2802201304248230897539.jpg', '<p>\r\n	Telhas Ceramicas, v&aacute;rios modelos:</p>\r\n<div style="page-break-after: always;">\r\n	<span style="display: none;">&nbsp;</span></div>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<strong>&nbsp;</strong><strong>Plan, &nbsp; &nbsp; &nbsp; &nbsp;</strong><strong>Romana, &nbsp; &nbsp; &nbsp; &nbsp;</strong><strong>Portuguesa, &nbsp; &nbsp; &nbsp;&nbsp;</strong><strong>Francesa, &nbsp; &nbsp;&nbsp;</strong><strong>Colonial, &nbsp; &nbsp; &nbsp;</strong><strong>Cumeeira</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	CARACTER&Iacute;STICAS T&Eacute;CNICAS: &nbsp; &nbsp; &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Telha Plan &nbsp; Cores:</p>\r\n<p>\r\n	Vermelha, Vermelha Mesclada &nbsp;</p>\r\n<p>\r\n	Rendimento por m&sup2;:&nbsp;21 a 26 pe&ccedil;as&nbsp;</p>\r\n<p>\r\n	Inclina&ccedil;&atilde;o m&iacute;nima:&nbsp;25%&nbsp;</p>\r\n<p>\r\n	Peso por m&sup2;:&nbsp;52 kg &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Telha Romana &nbsp; Cores:</p>\r\n<p>\r\n	Vermelha, Vermelha Mesclada, Cores Esmaltadas &nbsp;</p>\r\n<p>\r\n	Rendimento por m&sup2;:&nbsp;16 pe&ccedil;as</p>\r\n<p>\r\n	&nbsp;Inclina&ccedil;&atilde;o m&iacute;nima:30%&nbsp;</p>\r\n<p>\r\n	Peso por m&sup2;:&nbsp;44 kg &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Telha Portuguesa &nbsp; Cores:</p>\r\n<p>\r\n	Vermelha, Vermelha Mesclada, Branca, Branca Mesclada &nbsp;</p>\r\n<p>\r\n	Rendimento por m&sup2;:&nbsp;17 pe&ccedil;as</p>\r\n<p>\r\n	&nbsp;Inclina&ccedil;&atilde;o m&iacute;nima:&nbsp;30%&nbsp;</p>\r\n<p>\r\n	Peso por m&sup2;:&nbsp;44 kg &nbsp; &nbsp; &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Telha Francesa &nbsp; Cores:</p>\r\n<p>\r\n	Vermelha, Vermelha Mesclada, Branca, Branca Mesclada &nbsp;</p>\r\n<p>\r\n	Rendimento por m&sup2;:&nbsp;17 pe&ccedil;as&nbsp;</p>\r\n<p>\r\n	Inclina&ccedil;&atilde;o m&iacute;nima: 40%&nbsp;</p>\r\n<p>\r\n	Peso por m&sup2;:&nbsp;44 kg &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Telha Colonial &nbsp; Cores:</p>\r\n<p>\r\n	Vermelha, Vermelha Mesclada, Branca, Branca Mesclada &nbsp;</p>\r\n<p>\r\n	Rendimento por m&sup2;:&nbsp;09 a 26 pe&ccedil;as&nbsp;</p>\r\n<p>\r\n	Inclina&ccedil;&atilde;o m&iacute;nima:&nbsp;25%&nbsp;</p>\r\n<p>\r\n	Peso por m&sup2;:&nbsp;51 kg &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Telha Cumeeira &nbsp; Cores:</p>\r\n<p>\r\n	Vermelha, Vermelha Mesclada, Branca Mesclada &nbsp;</p>\r\n<p>\r\n	Rendimento por metro linear: ............................................. 3 pe&ccedil;as</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Telha Italiana &nbsp; Cores:</p>\r\n<p>\r\n	Vermelha, Vermelha Mesclada, Branca, Branca Mesclada, Cores Esmaltadas &nbsp;</p>\r\n<p>\r\n	Rendimento por m&sup2;:&nbsp;14,5 pe&ccedil;as&nbsp;</p>\r\n<p>\r\n	Inclina&ccedil;&atilde;o m&iacute;nima: 30%&nbsp;</p>\r\n<p>\r\n	Peso por m&sup2;:&nbsp;45 kg &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Telha Americana &nbsp; Cores:</p>\r\n<p>\r\n	Vermelha, Vermelha Mesclada, Branca, Branca Mesclada &nbsp;</p>\r\n<p>\r\n	Rendimento por m&sup2;:&nbsp;12 pe&ccedil;as&nbsp;</p>\r\n<p>\r\n	Inclina&ccedil;&atilde;o m&iacute;nima:&nbsp;30%&nbsp;</p>\r\n<p>\r\n	Peso por m&sup2;:&nbsp;38 kg</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>', 'NAO', 0, '', ''),
(8, 'Fechaduras', '1706201607213809812532.jpg', '<p style="text-align: justify;">\r\n	A <strong>Atacad&atilde;o da Madeira</strong> vende fechaduras, dobradi&ccedil;as, puxadores, cadeados, ma&ccedil;anetas, cilindros, molas a&eacute;reas, dentre outros acess&oacute;rios para portas de entrada, pivotantes e de correr, sempre visando o melhor atendimento e a satisfa&ccedil;&atilde;o dos clientes.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Para maiores informa&ccedil;&otilde;es entre em contato conosco e solicite um or&ccedil;amento.</p>', 'SIM', 0, '', ''),
(10, 'Madeirite', '1807201612515502265381.jpg', '<p style="text-align: justify;">\r\n	A <strong>Atacad&atilde;o da Madeira </strong>disponibiliza em nosso estoque formas resinadas, formas para concreto, tapume, madeirites, compensados resinados para constru&ccedil;&atilde;o. As Chapas de Madeirite resinadas tem a medida padr&atilde;o de 2,20m x 1,10m com a colagem em cola branca e colagem em cola fen&oacute;lica 100% &agrave; prova d&acute;&aacute;gua, superf&iacute;cies acabadas em ambas as faces e topos selados com resina impermeabilizante.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Dimens&atilde;o: 2,20m x 1,10m</p>\r\n<p style="text-align: justify;">\r\n	Espessuras: &nbsp;6 mm / 10 mm / 12 mm / 14 mm / &nbsp;17 mm</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Para maiores informa&ccedil;&otilde;es entre em contato conosco e solicite um or&ccedil;amento.</p>\r\n<p>\r\n	&nbsp;</p>', 'SIM', 0, '', ''),
(11, 'MDF Branco TX', '1706201607402873076308.jpg', '<p style="text-align: justify;">\r\n	A <strong>Atacad&atilde;o da Madeira</strong> disponibiliza em nosso estoque chapas em MDF com pintura na cor branca. Excelente acabamento.</p>\r\n<p style="text-align: justify;">\r\n	Reduza os custos na sua ind&uacute;stria de m&oacute;veis. Utiliza&ccedil;&atilde;o de madeira reflorestada e ecologicamente correta.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Dispon&iacute;vel nas espessuras: 2,5 / 3,0 e 6,0 mm.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Para maiores informa&ccedil;&otilde;es entre em contato conosco e solicite um or&ccedil;amento.</p>', 'SIM', 0, '', ''),
(13, 'Tuturuba', '2802201311317382654593.jpg', '<p>\r\n	&nbsp;</p>\r\n<p style="margin: 0px auto 10px; list-style: none; outline: none; border: none; text-align: justify; font-family: Ubuntu, sans-serif;">\r\n	Disponibilizamos para nossos clientes&nbsp;as mais diversas madeiras, legalizadas e com certificado de extra&ccedil;&atilde;o.</p>\r\n<p style="margin: 0px auto 10px; list-style: none; outline: none; border: none; text-align: justify; font-family: Ubuntu, sans-serif;">\r\n	&nbsp;</p>\r\n<div>\r\n	&nbsp;</div>', 'NAO', 0, '', ''),
(14, 'Portas de Ipê', '1706201608538331216350.jpg', '<p style="text-align: justify;">\r\n	A <strong>Atacad&atilde;o da Madeira</strong> disponibiliza em nosso estoque portas com cor e material de ip&ecirc; e superf&iacute;cie lisa.</p>\r\n<p style="text-align: justify;">\r\n	Produto ecologicamente correto &ndash; madeira de reflorestamento.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Para maiores informa&ccedil;&otilde;es entre em contato conosco e solicite um or&ccedil;amento.</p>', 'SIM', 0, '', ''),
(15, 'Telhas Americanas Ouro Branco', '1706201609516785530774.jpg', '<p style="text-align: justify;">\r\n	A Atacad&atilde;o da Madeira est&aacute; sempre atenta &agrave;s novas marcas e lan&ccedil;amentos do mercado cer&acirc;mico, visando disponibiliz&aacute;-las aos clientes que procuram sempre por mais seguran&ccedil;a, beleza, requinte e o bem-estar das pessoas.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Oferecemos a maior variedade de cores de telhas da linha Americana Ouro Branco&reg;.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Cer&acirc;mica Ouro Branco Ltda - Oferecendo telhas cer&acirc;micas de alta qualidade, que provoquem um agrad&aacute;vel impacto visual, tornando sua obra exclusiva - www.ouroblanco.com.br</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Para maiores informa&ccedil;&otilde;es entre em contato conosco e solicite um or&ccedil;amento.</p>', 'SIM', 0, '', ''),
(17, 'Telhas de Concreto Decorlit', '1706201610197010940959.jpg', '<p style="text-align: justify;">\r\n	A&nbsp;<strong>Atacad&atilde;o da Madeira</strong>&nbsp;oferece a maior variedade de cores de telhas de concreto DECORLIT&reg;. Como resultado, voc&ecirc; recebe uma Telha de Concreto confi&aacute;vel, com design moderno e custo acess&iacute;vel, aprovado por arquitetos, engenheiros e consumidores de todo o pa&iacute;s.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Com telhas de concreto da DECORLIT&reg; sua obra fica valorizada e bem vista - www.decorlit.com.br</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Para maiores informa&ccedil;&otilde;es entre em contato conosco e solicite um or&ccedil;amento.</p>', 'SIM', 0, '', ''),
(18, 'Placa Cimentícia Decorlit', '1706201610321192982639.jpg', '<p>\r\n	A <strong>Atacad&atilde;o da Madeira </strong>oferece A PLACA CIMENT&Iacute;CIA DECORLIT, a primeira placa impermeabilizada e prensada que utiliza tecnologia CCRFS (tecnologia sem amianto). &Eacute; uma placa com menor varia&ccedil;&atilde;o dimensional, com flexibilidade e mais suave na aplica&ccedil;&atilde;o dos parafusos. Utilizada principalmente no fechamento do sistema Steel Frame, revestimento de colunas e vigas met&aacute;licas, decora&ccedil;&atilde;o e design, proporcionando excelente acabamento e qualidade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DECORLIT&reg; sua obra fica valorizada e bem vista - www.decorlit.com.br</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para maiores informa&ccedil;&otilde;es entre em contato conosco e solicite um or&ccedil;amento.</p>', 'SIM', 0, '', ''),
(19, 'Assoalho de Ipê', '0107201611473537427162.jpg', '<p>\r\n	Disponibilizamos Assoalhos em Madeira Ip&ecirc; muito pesada e muito dura ao corte, na qualidade extra ou comercial, usada para acabamentos na parte interna e externa, conforme sua necessidade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para maiores informa&ccedil;&otilde;es entre em contato conosco e solicite um or&ccedil;amento.</p>', 'SIM', 0, '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE `tb_servicos` (
  `idservico` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `imagem` varchar(45) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `keywords` longtext NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `imagem`, `descricao`, `ativo`, `ordem`, `keywords`, `description`) VALUES
(3, 'Madeiras para Obras Residenciais', '1003201310547877634867.png', '<p>\r\n	A Atacad&atilde;o da Madeira disponibiliza para nossos clientes as mais diversas madeiras, seja para constru&ccedil;&atilde;o residencial ou comercial, legalizadas e com certificado de extra&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para maiores informa&ccedil;&otilde;es entre em contato conosco e solicite um or&ccedil;amento.</p>', 'SIM', 0, '', ''),
(4, 'Pergolados', '2802201311368837369411.jpg', '<p>\r\n	Na Atacad&atilde;o da Madeira voc&ecirc; encontra as mais diversas madeiras, legalizadas e com certificado de extra&ccedil;&atilde;o, para constru&ccedil;&atilde;o de estruturas de Pergolados ou Gazebos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para maiores informa&ccedil;&otilde;es entre em contato conosco e solicite um or&ccedil;amento.</p>', 'SIM', 0, '', ''),
(11, 'Madeiras e Telhas para Telhados', '0107201612351269524411.jpg', '<p>\r\n	Oferecemos a maior variedade de cores de telhas da linha Americana Ouro Branco&reg; ou telhas de concreto DECORLIT&reg;. Oferecendo telhas cer&acirc;micas de alta qualidade, que provoquem um agrad&aacute;vel impacto visual, tornando sua obra exclusiva.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Atacad&atilde;o da Madeira tamb&eacute;m disponibiliza para nossos clientes as mais diversas madeiras para a constru&ccedil;&atilde;o de seu telhado, desde escoras a vigas de suporte.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para maiores informa&ccedil;&otilde;es entre em contato conosco e solicite um or&ccedil;amento.</p>', 'SIM', 0, '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `OLD_tb_banners`
--
ALTER TABLE `OLD_tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Indexes for table `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Indexes for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Indexes for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `tb_enderecos`
--
ALTER TABLE `tb_enderecos`
  ADD PRIMARY KEY (`idendereco`);

--
-- Indexes for table `tb_galeria_produto`
--
ALTER TABLE `tb_galeria_produto`
  ADD PRIMARY KEY (`idgaleria`);

--
-- Indexes for table `tb_grupos_logins`
--
ALTER TABLE `tb_grupos_logins`
  ADD PRIMARY KEY (`idgrupologin`);

--
-- Indexes for table `tb_grupos_logins_tb_paginas`
--
ALTER TABLE `tb_grupos_logins_tb_paginas`
  ADD PRIMARY KEY (`id_grupologin`,`id_pagina`);

--
-- Indexes for table `tb_info_empresa`
--
ALTER TABLE `tb_info_empresa`
  ADD PRIMARY KEY (`idinfo`);

--
-- Indexes for table `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Indexes for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Indexes for table `tb_modulos_paginas`
--
ALTER TABLE `tb_modulos_paginas`
  ADD PRIMARY KEY (`idmodulopagina`);

--
-- Indexes for table `tb_paginas`
--
ALTER TABLE `tb_paginas`
  ADD PRIMARY KEY (`idpagina`);

--
-- Indexes for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `OLD_tb_banners`
--
ALTER TABLE `OLD_tb_banners`
  MODIFY `idbanner` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_enderecos`
--
ALTER TABLE `tb_enderecos`
  MODIFY `idendereco` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_galeria_produto`
--
ALTER TABLE `tb_galeria_produto`
  MODIFY `idgaleria` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `tb_grupos_logins`
--
ALTER TABLE `tb_grupos_logins`
  MODIFY `idgrupologin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_info_empresa`
--
ALTER TABLE `tb_info_empresa`
  MODIFY `idinfo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1304;
--
-- AUTO_INCREMENT for table `tb_modulos_paginas`
--
ALTER TABLE `tb_modulos_paginas`
  MODIFY `idmodulopagina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tb_paginas`
--
ALTER TABLE `tb_paginas`
  MODIFY `idpagina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;
--
-- AUTO_INCREMENT for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
