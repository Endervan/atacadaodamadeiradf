<?php 
require_once('Dao.class.php');
require_once('Util.class.php');

class Paginacao extends Dao
{
	public function get_paginacao_registro($pagina, $maximo_registro, $campos_sql, $final_sql)
	{
		// Declara��o da pagina inicial  
		if($pagina == "") {  
			$pagina = "1";  
		} 
		
	
		// Calculando o registro inicial  
		$inicio = $pagina - 1;  
		$inicio = $maximo_registro * $inicio;
		
		
		
		// Conta os resultados no total da minha query  
		$sql = "SELECT COUNT(*) AS 'numero_registros' $final_sql";  
		$result = parent::executaSQL($sql);
		$row = mysql_fetch_array($result);  
		$total = $row["numero_registros"];
		
		//	VERIFICO SE RETORNOU ALGO
		if($total<=0) 
		{  
			$sql = "SELECT $campos_sql $final_sql LIMIT $inicio, $maximo_registro";  
			$result = parent::executaSQL($sql);	
			return $result;
		} 
		else 
		{  
			$sql = "SELECT $campos_sql $final_sql LIMIT $inicio, $maximo_registro";  
			$result = parent::executaSQL($sql);	
			return $result;
		}				
	}
	
	
	
	
	public function html_links_paginacao($pagina, $maximo_registro, $final_sql, $complemento_link)
	{
		// verificio se � a primeira pagina
		if($pagina == "") {  
			$pagina = "1";  
		} 
		
		// Conta os resultados no total da minha query  
		$sql = "SELECT COUNT(*) AS 'numero_registros' $final_sql";  
		$result = parent::executaSQL($sql);
		$row = mysql_fetch_array($result);  
		$total = $row["numero_registros"];
		
		
		// Calculando pagina anterior  
		$menos = $pagina - 1;  
	
		// Calculando pagina posterior  
		$mais = $pagina + 1;
	
		// Retorna o pr�ximo maior valor inteiro arredondando para cima
		$pgs = ceil($total / $maximo_registro);  
		
		if($pgs > 1 ) {  
	
			$html .= '<div align=center>';				
				// Mostragem de pagina  
				if($menos > 0) {  
				   $html .= "<a href=\"?pagina=$menos$complemento_link\" >Anterior</a> ";  
				}  
				// Listando as paginas  
				for($i=1;$i <= $pgs;$i++) {  
					if($i != $pagina) {  
					   $html .= "  <a href=\"?pagina=".($i)."$complemento_link\" >$i</a>";  
					} else {  
						$html .= "  <strong class='link_atual_paginacao'>".$i."</strong>";  
					}  
				}  
				if($mais <= $pgs) {  
				   $html .= "   <a href=\"?pagina=$mais$complemento_link\" >proxima</a>";  
				}  		
			$html .= '</div>';
			
		}
		
		return $html;
	}
	
	
	
	
	public function get_links_paginacao($pagina, $maximo_registro, $final_sql, $complemento_link = '', $listar_pagina = 'SIM')
	{
		// verificio se � a primeira pagina
		if($pagina == "") {  
			$pagina = "1";  
		} 
		
		// Calculando o registro inicial  
		$inicio = $pagina - 1;  
		$inicio = $maximo_registro * $inicio;
		
		
		// Conta os resultados no total da minha query  
		$sql = "SELECT COUNT(*) AS 'numero_registros' $final_sql";  
		$result = parent::executaSQL($sql);
		$row = mysql_fetch_array($result);  
		$total = $row["numero_registros"];
		
		
		// Calculando pagina anterior  
		$menos = $pagina - 1;  
	
		// Calculando pagina posterior  
		$mais = $pagina + 1;
	
		// Retorna o pr�ximo maior valor inteiro arredondando para cima
		$pgs = ceil($total / $maximo_registro);  
		
		if($pgs > 1 ) {  
	
			echo '<div align="center" style="width:300px; margin:20px auto;">';
				// Mostragem de pagina  
				if($menos > 0) {  
				   echo "<a href=\"?pagina=$menos$complemento_link\" class='texto_paginacao_voltar'>
							<span>Anterior</span>
						</a> ";  
				}
				else
				{
					echo "<a href=\"javascript:void(0);\" class='texto_paginacao_voltar'>
							<span>Anterior</span>
						</a> ";
				}
				
				
				if($inicio == 0)
				{
					$registro_atual = 1;
				}
				else
				{
					$registro_atual = $inicio + 1;
				}
				
				echo "<span class='pagina_pagina_atual'>$registro_atual</span> <span class='paginacao_de'>de</span> <span class='paginacao_de'>$total</span>";  
				
				
				
				
				
					
					
				if($mais <= $pgs) {  
				   echo "   <a href=\"?pagina=$mais$complemento_link\" class='texto_paginacao_avancar'>
				   				<span>proxima</span>
							</a>";  
				}  	
				else
				{
					echo "   <a href=\"javascript:void(0);\" class='texto_paginacao_avancar'>
				   				<span>proxima</span>
							</a>";  
				}
				
					
			echo '</div>';
			
		}
	}
	
	
	
	public function total_registros()
	{
		// Conta os resultados no total da minha query  
		$sql = "SELECT COUNT(*) AS 'numero_registros' $final_sql";  
		$result = parent::executaSQL($sql);
		$row = mysql_fetch_array($result);  
		$total = $row["numero_registros"];	
	}
	
}
?>