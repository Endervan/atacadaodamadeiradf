﻿<?php
require_once("../class/Include.class.php"); 
require_once('../class/recaptchalib.php');
$obj_site = new Site();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("../includes/head.php"); ?>
</head>

<body>
<?php include("../includes/menu.php"); ?>

<div id="tudo">

	<div id="info_pagina">
    	
        <div class="navegacao">
        	<a href="<?php echo Util::caminho_projeto(); ?>">Home</a> >
            <a href="<?php echo Util::caminho_projeto(); ?>/contato"><span>Contato</span></a>
        </div>
        
        <h4>Contato</h4>
        
        
    </div>
	
	<div id="conteudo">
    	
        <div id="princ">
        	<?php
			$result = $obj_site->select("tb_enderecos");
			
			$num_rows = mysql_num_rows($result);
			
			if($num_rows > 0)
			{
				if($num_rows <= 1)
				{
					$height = "430";
				}
				else
				{
					$height = "200";
				}
				while($row = mysql_fetch_array($result))
				{
					?>
					<div class="endereco">
						<h2>
							<?php Util::imprime($row['titulo']); ?> - 
							<?php Util::imprime($row['endereco']); ?> - 
							<?php Util::imprime($row['telefone']); ?>
                        </h2>
						<iframe width="650" height="<?php echo $height; ?>"
                        		frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                                src="https://maps.google.com.br/maps?q=<?php Util::imprime($row['latitude']); ?>,<?php Util::imprime($row['longitude']); ?>&amp;num=1&amp;ie=UTF8&amp;t=m&amp;ll=-15.833049,-48.056259&amp;spn=0.016515,0.055704&amp;z=14&amp;output=embed"></iframe>
					</div>
					<?php
				}
			}
			?>
        </div>
        
        <div id="aux">
			<?php  
            //	VERIFICO SE E PARA ENVIAR O EMAIL
            if(isset($_POST[nome]))
            {
				if (!Util::valida_captcha()) {
						Util::script_msg(utf8_decode("Por favor, digite o que você vê na imagem"));
				    } else {
						$nome_remetente = ($_POST[nome]);
						$assunto = ($_POST[assunto]);
						$email = ($_POST[email]);
						$mensagem = (nl2br($_POST[mensagem]));
						$telefone = ($_POST[telefone]);
						
						$texto_mensagem = "
										  Nome: $nome_remetente <br />
										  Assunto: $assunto <br />
										  Telefone: $telefone <br />
										  Email: $email <br />
										  Mensagem:	<br />
										  $mensagem
										  ";
						Util::envia_email("marciomas@gmail.com", utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
						Util::envia_email("angela.homeweb@gmail.com", utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
						Util::envia_email("atacadaodamadeira@hotmail.com", utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
						Util::envia_email("junior@homewebbrasil.com.br", utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);


						
						?>
						<!-- Google Code for Or&ccedil;amento Conversion Page -->
						<script type="text/javascript">
						/* <![CDATA[ */
						var google_conversion_id = 1004852054;
						var google_conversion_language = "en";
						var google_conversion_format = "2";
						var google_conversion_color = "ffffff";
						var google_conversion_label = "Ch-TCPrMxQUQ1qaT3wM";
						var google_conversion_value = 0;
						/* ]]> */
						</script>
						<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
						</script>
						<noscript>
						<div style="display:inline;">
						<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1004852054/?value=0&amp;label=Ch-TCPrMxQUQ1qaT3wM&amp;guid=ON&amp;script=0"/>
						</div>
						</noscript>
						<?php
						
						Util::script_msg("Obrigado por entrar em contato.");
					}
            }
            ?>
            
            <form id="form_contato" name="form_contato" method="post" action="">
            	
                <label for="nome">Nome: </label>
                <input type="text" name="nome" id="nome" alt="Campo Nome" title="Campo Nome" />
                
                <label for="assunto">Assunto: </label>
                <input type="text" name="assunto" id="assunto" alt="Campo Assunto" title="Campo Assunto" />
                
                <label for="email">E-mail: </label>
                <input type="text" name="email" id="email" alt="Campo E-mail" title="Campo E-mail" />
                
                <label for="telefone">Telefone: </label>
                <input type="text" name="telefone" id="telefone" alt="Campo Telefone" title="Campo Telefone" />
                
                <label for="mensagem">Mensagem: </label>
                <textarea name="mensagem" rows="5" id="mensagem" title="Campo Mensagem"></textarea>
                
				<div>
					<label>*Digite o que você vê na imagem abaixo.</label>
					<?php Util::gera_captcha(); ?>
				</div>
				
                <input type="submit" name="enviar" id="enviar" value="Enviar" alt="Enviar Formulário" />
            </form>
        </div>
        
    </div>
    
    <div class="clear"></div>

</div>

<?php include("../includes/rodape.php"); ?>

</body>
</html>


<?php include("../includes/js_css.php"); ?>
