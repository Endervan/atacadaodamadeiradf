﻿<?php
require_once("../class/Include.class.php"); 
$obj_site = new Site();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("../includes/head.php"); ?>

<?php
// BUSCA A INTERNA DA PAGINA
$parametro = explode("-",$_REQUEST['get1']);
$iddica = addslashes($parametro[0]);
$row_dica = $obj_site->select_unico("tb_dicas","iddica" ,$iddica);
?>

<meta name="description" content="<?php Util::imprime($row_dica[description]); ?>" />
<meta name="keywords" content="<?php Util::imprime($row_dica[keywords]); ?>" />
<meta name="revisit-after" content="7 Days" />
<meta name="language" content="pt-br" /> 
<meta name="robots" content="all" /> 
<meta name="rating" content="general" /> 
<meta name="copyright" content="Copyright Masmidia www.masmidia.com.br" /> 

</head>

<body>
<?php include("../includes/menu.php"); ?>

<div id="tudo">

	<div id="info_pagina">
    	
        <div class="navegacao">
        	<a href="<?php echo Util::caminho_projeto(); ?>">Home</a> >
            <a href="<?php echo Util::caminho_projeto(); ?>/dicas"><span>dicas</span></a>
        </div>
        
        <h4>Dicas</h4>
        
    </div>
	
	<div id="conteudo">
    	
		<h1><?php Util::imprime($row_dica['titulo']); ?></h1>
        
        <div class="banner_lateral">
        	<div class="banner_lateral_img">
            	<img src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($row_dica['imagem']); ?>" alt="<?php Util::imprime($row_dica['titulo']); ?>" title="<?php Util::imprime($row_dica['titulo']); ?>" />
            </div>
        </div>
        
        <div class="content_descricao">
            <?php Util::imprime($row_dica['descricao']); ?>
        </div>

        
    </div>

    <div class="clear"></div>

</div>

<?php include("../includes/rodape.php"); ?>

</body>
</html>

<?php include("../includes/js_css.php"); ?>