﻿<?php
require_once("../class/Include.class.php"); 
$obj_site = new Site();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("../includes/head.php"); ?>

<?php
// BUSCA O RESULTADO NA TABELA COM O QUE PERTENCE AO TEXTO DA EMPRESA
$result_pagempresa = $obj_site->select("tb_empresa","AND idempresa = 1");
$row_pagempresa = mysql_fetch_array($result_pagempresa )
?>

<meta name="description" content="<?php Util::imprime($row_pagempresa[description]); ?>" />
<meta name="keywords" content="<?php Util::imprime($row_pagempresa[keywords]); ?>" />
<meta name="revisit-after" content="7 Days" />
<meta name="language" content="pt-br" /> 
<meta name="robots" content="all" /> 
<meta name="rating" content="general" /> 
<meta name="copyright" content="Copyright Masmidia www.masmidia.com.br" /> 

</head>

<body>
<?php include("../includes/menu.php"); ?>

<div id="tudo">

	<div id="info_pagina">
    	
        <div class="navegacao">
        	<a href="<?php echo Util::caminho_projeto(); ?>">Home</a> >
            <a href=""><span>Empresa</span></a>
        </div>
        
        <h4>Empresa</h4>
        
    </div>
	
	<div id="conteudo">
    	
        
        <div class="banner_lateral">
            <img src="<?php echo Util::caminho_projeto(); ?>/uploads/tumb_<?php Util::imprime($row_pagempresa['imagem']); ?>" 
            	 alt="<?php Util::imprime($row_pagempresa['titulo']); ?>" title="<?php Util::imprime($row_pagempresa['titulo']); ?>" />
        </div>
        
        <div class="content_descricao">
            <?php Util::imprime($row_pagempresa['descricao']); ?>
        </div>

        
    </div>

    <div class="clear"></div>

</div>

<?php include("../includes/rodape.php"); ?>

</body>
</html>

<?php include("../includes/js_css.php"); ?>