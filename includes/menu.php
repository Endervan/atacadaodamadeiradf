<div id="content_menu">
    <div id="menu">
        <div class="logo">
            <a href="<?php echo Util::caminho_projeto(); ?>" title="Home">
                <img src="<?php echo Util::caminho_projeto(); ?>/images/logo.png" title="Home" />
            </a>
        </div>
        
        <ul id="nav_menu">
            <li class="<?php echo $class_home ?>" ><a href="<?php echo Util::caminho_projeto(); ?>">Home</a></li>
            <li class="<?php echo $class_empresa ?>" ><a href="<?php echo Util::caminho_projeto(); ?>/empresa">Empresa</a></li>
            <li class="<?php echo $class_produto ?>" ><a href="<?php echo Util::caminho_projeto(); ?>/produtos">Produto</a></li>
            <li class="<?php echo $class_servicos ?>" ><a href="<?php echo Util::caminho_projeto(); ?>/servicos">Serviços</a></li>
            <li class="<?php echo $class_dicas ?>" ><a href="<?php echo Util::caminho_projeto(); ?>/dicas">Dicas</a></li>
            <li class="<?php echo $class_contato ?>" ><a href="<?php echo Util::caminho_projeto(); ?>/contato">Contato</a></li>
        </ul>
        
        
        <?php
		//	separa o telefone
		$telefone = explode("/", $row_empresa['telefone']);
		?>
        <div class="telefone">
    	    
	    	<h3><i class="fa fa-phone" aria-hidden="true"></i> <?php Util::imprime($telefone[0]); ?></h3>
       		<h3><i class="fa fa-phone" aria-hidden="true"></i> <?php Util::imprime($telefone[1]); ?></h3>
            <h3><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php Util::imprime($telefone[2]); ?></h3>

        </div>
        
    </div>
</div>