﻿<?php
require_once("class/Include.class.php");
$obj_site = new Site();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include("includes/js_css.php"); ?>


<?php include("includes/head.php"); ?>

<!-- EASY SLIDER -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto(); ?>/jquery/easyslider1.7/js/easySlider1.7.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#slider_dicas_home").easySlider({
			auto: true,
			continuous: true
		});
	});
</script>
 
</head>
<body class="index">
<?php include("includes/menu.php"); ?>

<div id="tudo">
	<?php include("includes/banner.php"); ?>

	<h1 class="titulo_pagina">Seja bem vindo ao site Atacadão da Madeira</h1>
    <p><?php Util::imprime($row_empresa[msg_saudacao]) ?></p>

    <?php include("includes/chamadas_internas.php"); ?>
    <?php include("includes/conteudo_home.php"); ?>
</div>

<?php include("includes/rodape.php"); ?>

</body>


<!-- NIVO SLIDER -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto(); ?>/jquery/nivo-slider/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript">
$(window).load(function() {
	$('#slider').nivoSlider({
		directionNav: false,
		/*pauseTime: 10000,*/
	});
});
</script>


</html>


