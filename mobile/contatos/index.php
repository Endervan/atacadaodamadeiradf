
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>


<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row">
			<div class="bg_verde">
				<div class="col-xs-6 top20 bottom15 ">
					<!-- ======================================================================= -->
					<!-- Breadcrumbs    -->
					<!-- ======================================================================= -->
					<div class="breadcrumb">
						<a class="home_personalizado" href="<?php echo Util:: caminho_projeto() ?>/mobile/">HOME</i></a>
						<a class="active"><b>CONTATOS</b></a>
					</div>
					<!-- ======================================================================= -->
					<!-- Breadcrumbs    -->
					<!-- ======================================================================= -->
				</div>

				<div class="col-xs-6 top30 bottom15  empresa_descricao top10 text-right">
					<h1>CONTATOS</h1>
				</div>
			</div>

  </div>
</div>



<div class="container">
  <div class="row">
    <!-- ======================================================================= -->
    <!-- CONTATOS  -->
    <!-- ======================================================================= -->
    <div class="col-xs-12 top25 contatos">

      <div class="media">

        <div class="media-body">
          <h1 class="right5">
            <img class="right5" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_phone_preto.png" alt="" />
            <?php Util::imprime($config['telefone']); ?>
          </h1>
        </div>

        <div class="media-left media-middle">
          <a href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_contatos1.png" alt="" />
          </a>
        </div>

      </div>

    </div>
    <!-- ======================================================================= -->
    <!-- CONTATOS  -->
    <!-- ======================================================================= -->

  </div>
</div>


  <div class="container">
    <div class="row produtos_destaques">

      <div class="col-xs-12 top950 padding0">

        <?php
            //  VERIFICO SE E PARA ENVIAR O EMAIL
            if(isset($_POST[nome]))
            {
          		$texto_mensagem = "
                                  Nome: ".($_POST[nome])." <br />
                                  Assunto: ".($_POST[assunto])." <br />
                                  Telefone: ".($_POST[telefone])." <br />
                                  Email: ".($_POST[email])." <br />
                                  Mensagem: <br />
                                  ".(nl2br($_POST[mensagem]))."
                                  ";



                //Util::envia_email($config[email], utf8_decode("Enviado Por - $_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
                //Util::envia_email($config[email_copia], utf8_decode("Enviado Por - $_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
								Util::envia_email("endvan@gmail.com", utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);


								Util::envia_email("marciomas@gmail.com", utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
								Util::envia_email("angela.homeweb@gmail.com", utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
								Util::envia_email("atacadaodamadeira@hotmail.com", utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
								Util::envia_email("junior@homewebbrasil.com.br", utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
								Util::alert_bootstrap("Obrigado por entrar em contato.");
                unset($_POST);
            }

            ?>

        <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data">
          <div class="fundo_formulario_fale">
            <!-- formulario orcamento -->

            <div class="col-xs-6 top15">
              <div class="form-group input100 has-feedback">
                <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                <span class="fa fa-user form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-6 top15">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                <span class="fa fa-envelope form-control-feedback top15"></span>
              </div>
            </div>

            <div class="clearfix">  </div>

            <div class="col-xs-6 top15">
              <div class="form-group  input100 has-feedback">
                <input type="tel" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                <span class="fa fa-phone form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-6 top15">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">
                <span class="fa fa-star form-control-feedback top15"></span>
              </div>
            </div>

            <div class="clearfix">  </div>


            <div class="col-xs-12 top15">
             <div class="form-group input100 has-feedback">
              <textarea name="mensagem" cols="30" rows="8" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
              <span class="fa fa-pencil form-control-feedback top15"></span>
            </div>
          </div>


          <!-- formulario orcamento -->
          <div class="col-xs-12 text-right">
            <div class="top15 bottom25">
              <button type="submit" class="btn btn-formulario" name="btn_contato">
                ENVIAR
              </button>
            </div>
          </div>

        </div>

        <!--  ==============================================================  -->
        <!-- FORMULARIO-->
        <!--  ==============================================================  -->
      </form>

    </div>

		<!--  ==============================================================  -->
		<!--MAPA-->
		<!--  ==============================================================  -->
		<div class="col-xs-12 top50">
			<?php
	$result = $obj_site->select("tb_enderecos");

	$num_rows = mysql_num_rows($result);

	if($num_rows > 0)
	{
		if($num_rows <= 1)
		{
			$height = "300";
		}
		else
		{
			$height = "200";
		}
		while($row = mysql_fetch_array($result))
		{
			?>
			<div class="bottom10 fale_conosco">
				<p>
				<?php Util::imprime($row['endereco']); ?>
				</p>

			</div>
			<iframe width="100%" height="<?php echo $height; ?>"
											frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
													src="https://maps.google.com.br/maps?q=<?php Util::imprime($row['latitude']); ?>,<?php Util::imprime($row['longitude']); ?>&amp;num=1&amp;ie=UTF8&amp;t=m&amp;ll=-15.833049,-48.056259&amp;spn=0.016515,0.055704&amp;z=14&amp;output=embed"></iframe>

		</div>
		<?php
	}
}
?>
		<!--  ==============================================================  -->
		<!--MAPA-->
		<!--  ==============================================================  -->

  </div>
</div>



<?php require_once('../includes/rodape.php'); ?>

</body>

</html>


<?php require_once('../includes/js_css.php'); ?>

<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
			assunto: {
			 validators: {
				 notEmpty: {

				 }
			 }
		 },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
       telefone: {
         validators: {
					 notEmpty: {},
           phone: {
               country: 'BR',
               message: 'Informe um telefone válido.'
           }
         },
       }
    }
  });
  });
</script>


