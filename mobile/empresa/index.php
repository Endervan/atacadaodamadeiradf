
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->



	<div class="container ">
		<div class="row">
		<div class="bg_verde">
			<div class="col-xs-6 top20 bottom15 ">
				<!-- ======================================================================= -->
				<!-- Breadcrumbs    -->
				<!-- ======================================================================= -->
				<div class="breadcrumb">
					<a class="home_personalizado" href="<?php echo Util:: caminho_projeto() ?>/mobile/">HOME</i></a>
					<a class="active"><b>EMPRESA</b></a>
				</div>
				<!-- ======================================================================= -->
				<!-- Breadcrumbs    -->
				<!-- ======================================================================= -->
			</div>

			<div class="col-xs-6 top30 bottom15  empresa_descricao top10 text-right">
				<h1>A EMPRESA</h1>
			</div>
		</div>

			<div class="col-xs-12 top30">
				<img src="<?php echo Util:: caminho_projeto() ?>/mobile/imgs/empresa01.jpg" alt="" />
			</div>

			<div class="clearfix"></div>

			<?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
			<div class="col-xs-12 top30 empresa_descricao_geral">
				<p>
					<?php Util::imprime($row[descricao]); ?>
				</p>

			</div>
		</div>
	</div>




	<?php require_once('../includes/rodape.php'); ?>

</body>

</html>


<?php require_once('../includes/js_css.php'); ?>
