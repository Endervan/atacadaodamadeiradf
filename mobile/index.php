
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();


?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>

  <!-- <script type="text/javascript">
    $(document).ready(function() {
      $('#Carousel').carousel({
        interval: 5000
      })
    });
  </script> -->



</head>

<body>

      <!-- ======================================================================= -->
      <!-- topo    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/topo.php') ?>
      <!-- ======================================================================= -->
      <!-- topo    -->
      <!-- ======================================================================= -->



        <!-- ======================================================================= -->
        <!-- BG HOME    -->
        <!-- ======================================================================= -->
        <div class="container">
          <div class="row carroucel-home">

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

              <!--  ==============================================================  -->
              <!-- ARRUSTE BOLINHAS E SETA DENTRO GRID PARA QLQ RESOLUCAO-->
              <!--  ==============================================================  -->
              <div class="container">
                <div class="row">
                  <div class="col-xs-12">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                      <?php
                      $result = $obj_site->select("tb_banners", "and tipo_banner = 2 order by rand() ");
                      if(mysql_num_rows($result) > 0){
                        $i = 0;
                        while ($row = mysql_fetch_array($result)) {
                          $imagens[] = $row;
                          ?>
                          <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
                          <?php
                          $i++;
                        }
                      }
                      ?>
                    </ol>



                  </div>
                </div>
              </div>
              <!--  ==============================================================  -->
              <!-- ARRUSTE BOLINHAS E SETA DENTRO GRID PARA QLQ RESOLUCAO-->
              <!--  ==============================================================  -->


              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">


                <?php
                if (count($imagens) > 0) {
                  $i = 0;
                  foreach ($imagens as $key => $imagem) {
                    ?>
                    <div class="item <?php if($i == 0){ echo "active"; } ?>">
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">

                 </div>
                 <?php
                 $i++;
               }
             }
             ?>

           </div>


         </div>

         <div class="container_banner_faixa"></div>

       </div>
      </div>
      <!-- ======================================================================= -->
      <!-- BG HOME    -->
      <!-- ======================================================================= -->

    <div class="container">
      <div class="row">
        <!-- ======================================================================= -->
        <!--     DESCRICAO HOME   -->
        <!-- ======================================================================= -->
      <?php $row = $obj_site->select_unico("tb_empresa","idempresa",5); ?>
        <div class="col-xs-12 top35 bottom20">
          <p>
            <?php Util::imprime($row[descricao]); ?>
          </p>
        </div>
        <!-- ======================================================================= -->
        <!--     DESCRICAO HOME   -->
        <!-- ======================================================================= -->

        
        

        <!-- ======================================================================= -->
        <!--     EMPRESA HOME   -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 top20">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/empresa_home.jpg" alt="" />
        </a>
        </div>
        <!-- ======================================================================= -->
        <!--     EMPRESA HOME   -->
        <!-- ======================================================================= -->

        <!-- ======================================================================= -->
        <!--     SERVIÇOS HOME   -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 top20">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/servico_home.jpg" alt="" />
        </a>
        </div>
        <!-- ======================================================================= -->
        <!--     SERVIÇOS HOME   -->
        <!-- ======================================================================= -->

        <!-- ======================================================================= -->
        <!--     CONTATOS HOME   -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 top20">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/contatos">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/contatos_home.jpg" alt="" />
        </a>
        </div>
        <!-- ======================================================================= -->
        <!--     CONTATOS HOME   -->
        <!-- ======================================================================= -->

      </div>
    </div>




    <!-- ======================================================================= -->
    <!--     SERVIÇOS HOME   -->
    <!-- ======================================================================= -->
    <div class="container">
      <div class="row">
        <div class="col-xs-12 top50">
            <div class="servicos_home">
              <h4><span>CONHEÇA MAIS NOSSOS SERVIÇOS</span></h4>
            </div>
        </div>


       <!-- repeticao ate 8 itens  -->
       <?php
       $result = $obj_site->select("tb_servicos", "order by rand() limit 8");
       if (mysql_num_rows($result) > 0) {
           while ($row = mysql_fetch_array($result)) {
           ?>
               <div class="col-xs-6 top15">
                  <div class="tipos_servicos_home">
                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php echo $obj_site->url($row[0], $row['titulo']) ?>">
                          <h1><i class="fa fa-angle-right right15 "></i><?php Util::imprime($row[titulo]) ?></h1>
                        </a>
                  </div>
                </div>
           <?php
           }
       }
       ?>
       
       
        

        
        
        

      </div>
    </div>
    <!-- ======================================================================= -->
    <!--     SERVIÇOS HOME   -->
    <!-- ======================================================================= -->


    <!-- ======================================================================= -->
    <!--     DICAS HOME   -->
    <!-- ======================================================================= -->
    <div class="container">
      <div class="row">
        <div class="col-xs-12 top50">
            <div class="servicos_home">
              <h4><span>DICAS</span></h4>
            </div>
        </div>



        <div class="col-xs-12 caroucel_dicas">
            <div id="carousel-example-generic1" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <?php
					unset($imagens);
				  $result = $obj_site->select("tb_dicas", "order by rand() limit 3 ");
				  if(mysql_num_rows($result) > 0){
					$i = 0;
					while ($row = mysql_fetch_array($result)) {
					  $imagens[] = $row;
					  ?>
					  <li data-target="#carousel-example-generic1" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
					  <?php
					  $i++;
					}
				  }
				  ?>
                </ol>


              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                  
                   <?php
					if (count($imagens) > 0) {
					  $i = 0;
					  foreach ($imagens as $key => $imagem) {
						?>
						<div class="item <?php if($i == 0){ echo "active"; } ?>">
						  <a href="<?php echo Util::caminho_projeto(); ?>/mobile/dica/<?php echo $obj_site->url($imagem[0], $imagem['titulo']) ?>" title="<?php Util::imprime($imagem[titulo]) ?>">
							<div class="col-xs-4 top15 padding0">
							  	<?php $obj_site->redimensiona_imagem("../uploads/$imagem[imagem]", 142, 87, array("class"=>"", "alt"=>"") ) ?>
							</div>

							<div class="col-xs-8 dicas_home">
							  <div class="top10">
								  <h1><?php Util::imprime($imagem[titulo]) ?></h1>
							  </div>
							  <div class="top15">
								<p><?php Util::imprime($imagem[descricao], 200) ?></p>
							  </div>
							</div>
						   </a>
					 	</div>
					 <?php
					 $i++;
				   }
				 }
				 ?>
                   


             </div>
            </div>
            <div class="borda_cinza top25">  </div>
         </div>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!--     DICAS HOME   -->
    <!-- ======================================================================= -->



  <?php require_once('./includes/rodape.php'); ?>

</body>

</html>


<?php require_once('./includes/js_css.php'); ?>



