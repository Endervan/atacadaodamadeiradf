﻿<?php
require_once("../class/Include.class.php"); 
$obj_site = new Site();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("../includes/head.php"); ?>

<?php
// BUSCA A INTERNA DA PAGINA
$parametro = explode("-",$_REQUEST['get1']);
$idproduto = addslashes($parametro[0]);
if(!empty($idproduto))
{
	$complemento = "AND idproduto = $idproduto";
}

$result = $obj_site->select("tb_produtos",$complemento);

if(mysql_num_rows($result) > 0)
{
	$row_produto = mysql_fetch_array($result);
}
?>

<meta name="description" content="<?php Util::imprime($row_produto[description]); ?>" />
<meta name="keywords" content="<?php Util::imprime($row_produto[keywords]); ?>" />
<meta name="revisit-after" content="7 Days" />
<meta name="language" content="pt-br" /> 
<meta name="robots" content="all" /> 
<meta name="rating" content="general" /> 
<meta name="copyright" content="Copyright Masmidia www.masmidia.com.br" /> 

</head>

<body>
<?php include("../includes/menu.php"); ?>

<div id="tudo">

	<div id="info_pagina">
    	
        <div class="navegacao">
        	<a href="<?php echo Util::caminho_projeto(); ?>">Home</a> >
            <a href="<?php echo Util::caminho_projeto(); ?>/produtos"><span>Produtos</span></a>
        </div>
        
        <h4>Produtos > <?php Util::imprime($row_produto[titulo]) ?></h4>
        
    </div>
	
	<div id="conteudo">
    	
		<h1 class="titulo_pagina" style="width:960px; margin:0 auto;"><?php Util::imprime($row_produto[titulo]) ?></h1>
		
        <div class="theme-pascal">
			<div class="banner_interna">
				<?php
				$result = $obj_site->select("tb_galeria_produto","and id_produto = $idproduto");
			
				if(mysql_num_rows($result) > 0)
				{
					while($row = mysql_fetch_array($result))
					{
						?>
						<img src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($row['imagem']); ?>" />
						<?php
					}
				}
				?>
			</div>
		</div>
        
        <div class="content_descricao">
            <?php Util::imprime($row_produto[descricao]) ?>
        </div>

        
    </div>

    <div class="clear"></div>

</div>

<?php include("../includes/rodape.php"); ?>



</body>
</html>




<?php include("../includes/js_css.php"); ?>


<script type="text/javascript" src="<?php echo Util::caminho_projeto(); ?>/jquery/nivo-slider/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript">
$(window).load(function() {
	$('.banner_interna').nivoSlider({
		directionNav: false,
	});
});
</script>