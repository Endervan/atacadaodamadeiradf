﻿<?php
require_once("../class/Include.class.php"); 
$obj_site = new Site();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("../includes/head.php"); ?>
</head>

<body>
<?php include("../includes/menu.php"); ?>

<div id="tudo">

	<div id="info_pagina">
    	
        <div class="navegacao">
        	<a href="<?php echo Util::caminho_projeto(); ?>">Home</a> >
            <a href="<?php echo Util::caminho_projeto(); ?>/servicos"><span>Serviços</span></a>
        </div>
        
        <h4>Serviços</h4>
        
        
    </div>
	
	<div id="conteudo">

        <ul id="lista_itens">
			
			<?php
			$result = $obj_site->select("tb_servicos");
		
			if(mysql_num_rows($result) > 0)
			{
				while($row = mysql_fetch_array($result))
				{
					?>
					<li>
						<div class="content_img">
							<img src="<?php echo Util::caminho_projeto(); ?>/uploads/tumb_<?php Util::imprime($row['imagem']); ?>" alt="<?php Util::imprime($row['titulo']); ?>" title="<?php Util::imprime($row['titulo']); ?>" />
						</div>
						<h1 class="titulo_pagina"><?php Util::imprime($row[titulo]); ?></h1>
						<?php Util::imprime($row[descricao],500); ?>
						<a href="<?php echo $obj_site->url($row['0'],$row['titulo']) ?>" class="titulo">Leia Mais</a>
					</li>
					<?php
				}
			}
			?>
		</ul>
        
    </div>
    
    <div class="clear"></div>

</div>

<?php include("../includes/rodape.php"); ?>

</body>
</html>

<?php include("../includes/js_css.php"); ?>
